import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'formulario.dart';
import 'home.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';


final _usernameController = TextEditingController();
final _passwordController = TextEditingController();


class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  double lat;
  double lon;

  @override
  void initState() {
    _read();
    super.initState();

    //timer = Timer.periodic(Duration(seconds: 15), (Timer t) => _getCurrentLocation());
  }

  @override
  void dispose() {
    //timer?.cancel();
    super.dispose();
  }

  @override
 Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(color:Color.fromRGBO(193, 29, 27, 1),),
      ),
      body: SafeArea(
        child: Container(
          color: Color.fromRGBO(255, 255, 255, 1),
              child: ListView(children: <Widget>[
                widgetTituloMobile(),
                widgetNombreUsuarioMobile(),
                wigdetContrasenaMobile(),
                wigdetBtnLoginMobile(),
                wigdetBtnRegistroMobile()
                ],
              ),
          )
      ),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }



   appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }

  Widget widgetTituloMobile(){
    return  Container(
            margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
              child:RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "Iniciar Sesión",
                  style: TextStyle(                     
                      color: Color.fromRGBO(193, 29, 27, 1),
                      fontSize: 50),
                ),
              )
  );
  }

Widget widgetNombreUsuarioMobile() {
  return Container(      
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 50.0),
      child: TextFormField(        
            controller: _usernameController,
            decoration: const InputDecoration(
              hintText: 'Usuario',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          ),
    );
}

Widget wigdetContrasenaMobile() {
  return Container(  
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 50.0),
      child: TextFormField(        
            controller: _passwordController,
            obscureText: true,
            decoration: const InputDecoration(
              hintText: 'Contraseña',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          ),
  );
}

Widget wigdetBtnLoginMobile() {
  return Container(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: 
          RaisedButton(
            onPressed: (){
             // _getCurrentLocation();
               if(_usernameController.text == "" || _passwordController.text == "") 
              {
            _campoVacio();
          }else{
              signIn(_usernameController.text, _passwordController.text);
              //Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePageLogin(token: "token")), (Route<dynamic> route) => false);
            }},
            child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "Ingresar",
                  style: TextStyle(                     
                      color: Color.fromRGBO(193, 29, 27, 1),
                      fontSize: 18),
                ),
              ),
            
          )
    );
}

Widget wigdetBtnRegistroMobile() {
  return Container(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
      child: 
          RaisedButton(
            color: Color.fromRGBO(193, 29, 27, 1),
            onPressed: (){
              _launchURL();
            },
            child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: "Registrarse",
                  style: TextStyle(                     
                      color: Color.fromRGBO(255, 255, 255, 1),
                      fontSize: 18),
                ),
              ),
          )
    );
}

_launchURL() async {
  const url = 'http://35.198.26.238:4000/accounts/signup/';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}







// FUNCIONES //


 Future<void> _campoVacio() async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Hay un campo vacio'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('Por favor llene todos los campos'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}







signIn(String usuario, pass)async{


Map data = {
    'username': usuario,
    'password': pass
  };
  var response = await http.post('http://50.116.46.197:8000/api/1.0/login/', body: data);
      
  //print(response.statusCode);
 
  var jsonResponse = json.decode(response.body);
  //print(response.statusCode);
  var token = "";
  var idUsuario;
  if(response.statusCode == 200){
    token = jsonResponse['token'];
    idUsuario = jsonResponse['id_user'];
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePageLogin(token: token, idUsuario: idUsuario)), (Route<dynamic> route) => false);
  
        
        final prefs = await SharedPreferences.getInstance();
        prefs.setStringList('datosInicio', [token, usuario, pass, idUsuario.toString()]);
       



    }else if(response.statusCode == 404){
      if (jsonResponse == "No existe el usuario"){
        _noUser();
 }else{
        token = jsonResponse['token'];
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => FormularioPage(username:usuario, password: pass, token: token, idUsuario: idUsuario, status: "new")), (Route<dynamic> route) => false);
    }
    }
    else if(response.statusCode == 401){
     _malIngreso();
  }
}

Future<void> _malIngreso() async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Hay uno o mas datos incorrectos'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('Por favor verifique la información y vuelva a intentarlo'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

Future<void> _noUser() async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('El usuario no existe'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('Por favor verifique la información o registre un usuario'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}


_read() async {
        final prefs = await SharedPreferences.getInstance();
        final myStringList = prefs.getStringList('datosInicio') ?? [];
        print('read: $myStringList');
        if(myStringList.isNotEmpty){
        signIn(myStringList[1], myStringList[2]);
        }
      }



}

