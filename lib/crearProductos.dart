import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'dart:math'; 
import 'package:siguelo_app/home.dart';

final _nombresController = TextEditingController();
final _precioController = TextEditingController();

class CrearProductosPage extends StatefulWidget {
  CrearProductosPage({
    Key key,
    this.idUsuario,
    this.token,
    this.idNegocio,
    this.producto,
    this.status
    
  }) : super(key: key);

  var idUsuario;
  var idNegocio;
  var producto;
  String token;
  String status;
  @override
  _CrearProductosPage createState() => _CrearProductosPage();
}

class _CrearProductosPage extends State<CrearProductosPage> {
//List pedidos = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  
  Future<File> file;
String status = '';
String base64Image;
File tmpFile;
String errMessage = 'Error Uploading Image';
String imagePath = "";


  chooseImage() {
    setState(() {
      file = ImagePicker.pickImage(source: ImageSource.gallery);
    });
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: ListView(
          children: <Widget>[
            widgetProductos(),
          ],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }

    Widget widgetTituloMobile() {
    return Container(
      padding: const EdgeInsets.only(bottom: 20.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Crear Producto",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 40),
          ),
        ));
  }


  widgetProductos(){

    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
        child: Column(children: <Widget>[
          widgetTituloMobile(),
          Container(
        padding: const EdgeInsets.only(bottom: 20.0),
        child: TextFormField(
            controller: _nombresController,
            decoration: const InputDecoration(
              hintText: 'Nombre del producto',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          )),
          Container(
        padding: const EdgeInsets.only(bottom: 20.0),
        child:
          Row(
            children: <Widget>[
              RaisedButton(
          onPressed: () {
         chooseImage();
          },
          child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Seleccionar archivo",
                style: TextStyle(
                    color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
              ),
            ),
        ),
        FutureBuilder<File>(
      future: file,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            null != snapshot.data) {
          tmpFile = snapshot.data;
          imagePath = tmpFile.path;
          base64Image = base64Encode(snapshot.data.readAsBytesSync());
          /*return Flexible(
            child: Image.file(
              snapshot.data,
              fit: BoxFit.scaleDown,
            ),
          );*/
          return Text(
            imagePath,
            textAlign: TextAlign.center,
          );
        } else if (null != snapshot.error) {
          return const Text(
            'Error',
            textAlign: TextAlign.center,
          );
        } else {
          return const Text(
            'No hay Imagen seleccionada',
            textAlign: TextAlign.center,
          );
        }
      },
    ),
        
            ]),
          ),
           Container(
        padding: const EdgeInsets.only(bottom: 20.0),
        child: TextFormField(
            controller: _precioController,
            decoration: const InputDecoration(
              hintText: 'Precio del producto',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          )),
          if(widget.status == "Crear")
          RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {
                      //sendRequest();
                      
                      crearProducto(_nombresController.text, _precioController.text);
                    },
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Crear",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  ),
                  if(widget.status == "Editar")
          RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {
                      //sendRequest();
                      
                      editarProducto(_nombresController.text, _precioController.text);
                    },
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Editar",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  ),
          
          ]));
  }


  
crearProducto(nombre, precio) async{


  


final mimeTypeData = lookupMimeType(tmpFile.path, headerBytes: [0xFF, 0xD8]).split('/');
var uri = Uri.parse('http://50.116.46.197:8000/api/1.0/producto/' + widget.idNegocio.toString() + '/create/');
var request = new http.MultipartRequest("POST", uri);

request.fields['nombre'] = nombre;
final file = await http.MultipartFile.fromPath(
    'foto',
    tmpFile.path,
    contentType: MediaType(mimeTypeData[0], mimeTypeData[1]),
);
request.fields['precio'] = precio;



request.files.add(file);
request.headers['Authorization'] = 'Token ' + widget.token.toString();

var streamedResponse = await request.send();
var response = await http.Response.fromStream(streamedResponse);
print(response.body);

if(response.statusCode == 201){
productoCreado();
}
}


  Future<void> productoCreado() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Producto Creado'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Tu producto se creo correctamente'),
              ],
            ),
          ),
          actions: <Widget>[

            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                     Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePageLogin( token: widget.token, idUsuario: widget.idUsuario)), (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }



editarProducto(nombre, precio) async{


  if(_nombresController.text == ""){
    nombre = (widget.producto["nombre"]);
  }

  if(_precioController.text == ""){
    precio = (widget.producto["precio"]);
  }


  if (tmpFile == null){

    var rng = new Random();
Directory tempDir = await getTemporaryDirectory();
String tempPath = tempDir.path;
File file = new File('$tempPath'+ (rng.nextInt(100)).toString() +'.png');
http.Response response = await http.get("http://" + widget.producto["foto"]);
await file.writeAsBytes(response.bodyBytes);
tmpFile = file;
  }


 

final mimeTypeData = lookupMimeType(tmpFile.path, headerBytes: [0xFF, 0xD8]).split('/');
var uri = Uri.parse('http://50.116.46.197:8000/api/1.0/producto/' + widget.producto["id"].toString() + '/');
var request = new http.MultipartRequest("PUT", uri);

request.fields['nombre'] = nombre;
final file = await http.MultipartFile.fromPath(
    'foto',
    tmpFile.path,
    contentType: MediaType(mimeTypeData[0], mimeTypeData[1]),
);
request.fields['precio'] = precio.toString();



request.files.add(file);
request.headers['Authorization'] = 'Token ' + widget.token.toString();

var streamedResponse = await request.send();
var response = await http.Response.fromStream(streamedResponse);
print(response.statusCode);

if(response.statusCode == 200){
productoEditado();
}
}


  Future<void> productoEditado() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Producto Editado'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Tu producto se edito correctamente'),
              ],
            ),
          ),
          actions: <Widget>[

            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                     Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePageLogin( token: widget.token, idUsuario: widget.idUsuario)), (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }
}