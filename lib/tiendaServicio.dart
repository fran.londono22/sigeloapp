import 'package:flutter/material.dart';
import './db/database_helpers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'agendarServicio.dart';
import 'carritoDeCompras.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:easy_dialog/easy_dialog.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart'; 

class TiendaPage extends StatefulWidget {
  TiendaPage(
      {Key key, this.username, this.password, this.token, this.idServicio, this.infoServicio, this.servicios})
      : super(key: key);

  String username;
  String password;
  String token;
  var idServicio;
  var infoServicio;
  var servicios;
  @override
  _TiendaPage createState() => _TiendaPage();
}

class _TiendaPage extends State<TiendaPage> {
  var info;
  var productos;
  var rating = 0.0;
  final _comentarioController = TextEditingController();



  @override
  void initState() {
    super.initState();    
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: new SingleChildScrollView(
        child: Column(
          children: <Widget>[wigdetPanelTiendaServicio(), widgetServicios()],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[widgetLogo(), widgetCarrito()]));
  }

  widgetCarrito() {
    return IconButton(
      icon: Icon(Icons.shopping_cart),
      color: Color.fromRGBO(193, 29, 27, 1),
      iconSize: 30.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CarritoPage(
                  username: widget.username,
                  password: widget.password,
                  token: widget.token)),
        );
      },
    );
  }

  wigdetPanelTiendaServicio() {
    return Column(
      children: <Widget>[
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: widget.infoServicio["nombre"],
            style: TextStyle(
                color: Color.fromRGBO(0, 0, 0, 1),
                fontSize: 30,
                fontWeight: FontWeight.bold),
          ),
        ),
        Image.network(
          widget.infoServicio["fachada"],
        ),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Dirección: ",
            style: TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
            children: <TextSpan>[
              TextSpan(
                  text: widget.infoServicio["direccion"],
                  style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          ),
        ),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Teléfono: ",
            style: TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
            children: <TextSpan>[
              TextSpan(
                  text: widget.infoServicio["telefono"],
                  style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          ),
        ),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Email: ",
            style: TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
            children: <TextSpan>[
              TextSpan(
                  text: widget.infoServicio["email_negocio"],
                  style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          ),
        ),
        RaisedButton(
                  color: Color.fromRGBO(28, 200, 138, 1),
                  onPressed: () {
                    FlutterOpenWhatsapp.sendSingleMessage(widget.infoServicio["telefono"], "Hola quisiera tener mas información del negoció");
                    comentario();
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      children: [
                        WidgetSpan(
                          child: 
                          Container(
                            width: 25,
                            height: 25,
                            child:Image.network(
                      'https://i.pinimg.com/originals/79/dc/31/79dc31280371b8ffbe56ec656418e122.png',
                    ))),
                        TextSpan(
                          text: " Enviar mensaje",
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 18),
                        ),                        
                      ],
                    ),
                  ),
                ),
        Divider(color: Colors.black),
      ],
    );
  }

  void comentario() {
    EasyDialog(
      cornerRadius: 15.0,
      fogOpacity: 0.1,
      width: 280,
      height: 280,
      contentPadding:
          EdgeInsets.only(top: 12.0), // Needed for the button design
      contentList: [
        Expanded(
            flex: 0,
          child: SmoothStarRating(
          rating: rating,
          isReadOnly: false,
          size: 30,
          filledIconData: Icons.star,
          halfFilledIconData: Icons.star_half,
          defaultIconData: Icons.star_border,
          starCount: 5,
          allowHalfRating: false,
          spacing: 2.0,
          onRated: (value) {
            setState(() {
              rating = value;
            });
            print("rating value -> $value");
            // print("rating value dd -> ${value.truncate()}");
          },
        )),
         Divider(color: Colors.black),
        Expanded(
            flex: 3,
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: TextFormField(
                controller: _comentarioController,
                maxLines: 5,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "Comenta tu experiencia",
                ),
              ),
            )),
        Container(
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.greenAccent
              ),
          child: FlatButton(
            onPressed: () {
              calificar();
              Navigator.of(context).pop();
            },
            child: Text(
              "Califícanos",
              textScaleFactor: 1.3,
            ),
          ),
        ),
        Container(
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.blueGrey,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0))),
          child: FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              "No hice pedido",
              textScaleFactor: 1.3,
            ),
          ),
        ),
      ]).show(context);
  }

  calificar() async{

    Map comentario = {
      'puntuacion': rating.toInt().toString(),
      'comentario': _comentarioController.text

    };

    print(comentario);

    final response = await http.post(
        'http://50.116.46.197:8000/api/1.0/servicios/calificacion/' +
            widget.idServicio.toString() +
            '/',
        body: comentario,
        headers: {
          'Authorization': 'Token ' + widget.token
        });

    print(response.body);

  }



  widgetServicios() {
    if (widget.servicios[1]["servicios"].isNotEmpty) {
     // for (var i = 0; i < productos[1]["productos"].length; i++){
      //print(widget.productos[1]["productos"].length);
      //print(widget.productos[1]["productos"]);
      return Expanded(
        child: ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemCount: widget.servicios[1]["servicios"].length,
          padding: const EdgeInsets.all(8),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                      color: Color.fromRGBO(54, 185, 204, 1),
                      width: MediaQuery.of(context).size.width,
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: widget.servicios[1]["servicios"][index]["nombre"],
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 20),
                        ),
                      )),
                  Image.network(
                    "http://" + widget.servicios[1]["servicios"][index]["foto"],
                  ),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Precio: ",
                      style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                      children: <TextSpan>[
                        TextSpan(
                            text: "\$" +
                                widget.servicios[1]["servicios"][index]["precio"]
                                    .toString(),
                            style: TextStyle(fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.only(
                          left: 20.0, top: 20.0, right: 20.0),
                      child: RaisedButton(
                        color: Color.fromRGBO(193, 29, 27, 1),
                        onPressed: () {

                          abrirAgenda(widget.servicios[1]["servicios"][index]["id"]);
                           

       
                        },
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: "Ver Agenda",
                            style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontSize: 18),
                          ),
                        ),
                      )),
                ],
              ),
            );
          }));
    //}
    } else {
      return Expanded(
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "No tiene servicios creados!",
            style: TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
          ),
        ),
      );
    }
  }

  abrirAgenda(id)async{
    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/servicios/' +
            id.toString() +
            '/',
        headers: {
          'Authorization': 'Token ' + widget.token,
          'Content-type': 'application/json'
        });
         var data = utf8.decode(response.bodyBytes);
    var servicioInfo = jsonDecode(data);

    final response1 = await http.get(
        'http://50.116.46.197:8000/api/1.0/servicios/' +
            id.toString() +
            '/schedule-servicio/listar/',
        headers: {
          'Authorization': 'Token ' + widget.token,
          'Content-type': 'application/json'
        });
         var data1 = utf8.decode(response1.bodyBytes);
    var servicioHorario = jsonDecode(data1);

    

         Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AgendaPage(
                              username: widget.username,
                              password: widget.password,
                              token: widget.token,
                              servicioInfo: servicioInfo,
                              servicioHorario: servicioHorario,
                              idServicio: id)),
                    );
  }


}
