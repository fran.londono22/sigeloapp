import 'dart:async';
import 'package:flutter/material.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'home.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:convert';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:math';
import 'package:siguelo_app/home.dart';

final _nombresController = TextEditingController();
final _dirController = TextEditingController();
final _telefonoController = TextEditingController();
final _mailController = TextEditingController();
final _descripcionController = TextEditingController();
final _costoController = TextEditingController();

String dropdownValue = 'Categoría';

class CrearNegocioPage extends StatefulWidget {
  CrearNegocioPage(
      {Key key, this.idUsuario, this.status, this.token, this.infoNegocio})
      : super(key: key);

  var idUsuario;
  String status;
  String token;
  var infoNegocio;

  @override
  _CrearNegocioPage createState() => _CrearNegocioPage();
}

class _CrearNegocioPage extends State<CrearNegocioPage>
    with SingleTickerProviderStateMixin {
  // Dio dio = new Dio();
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  Position _currentPosition;
  Placemark place;

  int _tabIndex = 0;
  TabController _tabController;

  Timer timer;
  String dir;

  bool domicilio = false;
  bool lugar = false;

  double lat;
  double lon;

  Future<File> file;
  String status = '';
  String base64Image;
  File tmpFile;
  String errMessage = 'Error Uploading Image';
  String imagePath = "";

  final picker = ImagePicker();

  chooseImage() {
    setState(() {
      file = ImagePicker.pickImage(source: ImageSource.gallery);
    });
  }

  _handleTabChange() {
    if (_dirController != null && _tabController.index == 2) {
      timer = Timer.periodic(Duration(seconds: 5), (Timer t) => _getLocation());
    }
  }

  @override
  void initState() {
    _getCurrentLocation();
    super.initState();
    _tabController = TabController(vsync: this, length: 3);
    _tabController.addListener(_handleTabChange);
  }

  @override
  void dispose() {
    _tabController.dispose();
    timer?.cancel();
    super.dispose();
  }

  void _toggleTab() {
    _tabIndex = _tabController.index + 1;
    _tabController.animateTo(_tabIndex);
  }

  void _toggleTab2() {
    _tabIndex = _tabController.index + 1;
    _tabController.animateTo(_tabIndex);
  }

  void _toggleTabPrev() {
    _tabIndex = _tabController.index - 1;
    _tabController.animateTo(_tabIndex);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color.fromRGBO(255, 255, 255, 1),
              title: appBarMobile(),
              iconTheme: new IconThemeData(
                color: Color.fromRGBO(193, 29, 27, 1),
              ),
            ),
            body: SafeArea(
              child: DefaultTabController(
                length: 3,
                child: Column(
                  children: <Widget>[
                    Container(
                      constraints: BoxConstraints(maxHeight: 150.0),
                      child: Material(
                        color: Colors.indigo,
                        child: TabBar(
                          controller: _tabController,
                          tabs: [
                            Tab(text: "1"),
                            Tab(text: "2"),
                            Tab(text: "3"),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: TabBarView(
                        physics: NeverScrollableScrollPhysics(),
                        controller: _tabController,
                        children: [
                          widgetFormulario(),
                          widgetDescripcion(),
                          widgetMapa()
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            )));
  }

  Future<bool> _onBackPressed() {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (BuildContext context) => HomePageLogin(
                token: widget.token, idUsuario: widget.idUsuario)),
        (Route<dynamic> route) => false);
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      Container(
          child: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                  builder: (BuildContext context) => HomePageLogin(
                      token: widget.token, idUsuario: widget.idUsuario)),
              (Route<dynamic> route) => false);
        },
      )),
      widgetLogo(),
    ]));
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  Widget widgetTituloMobile() {
    return Container(
        padding: const EdgeInsets.only(bottom: 20.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Registrar negocio",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 40),
          ),
        ));
  }

  Widget widgetFormulario() {
    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
        child: Column(children: <Widget>[
          widgetTituloMobile(),
          Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: TextFormField(
                controller: _nombresController,
                decoration: const InputDecoration(
                  hintText: 'Nombre del Negocio',
                  border: const OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white,
                ),
              )),
          Container(
            padding: const EdgeInsets.only(bottom: 20.0),
            child: Row(children: <Widget>[
              RaisedButton(
                onPressed: () {
                  chooseImage();
                },
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: "Seleccionar archivo",
                    style: TextStyle(
                        color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
                  ),
                ),
              ),
              FutureBuilder<File>(
                future: file,
                builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      null != snapshot.data) {
                    tmpFile = snapshot.data;
                    imagePath = tmpFile.path;
                    base64Image = base64Encode(snapshot.data.readAsBytesSync());
                    /*return Flexible(
            child: Image.file(
              snapshot.data,
              fit: BoxFit.scaleDown,
            ),
          );*/
                    return Text(
                      imagePath,
                      textAlign: TextAlign.center,
                    );
                  } else if (null != snapshot.error) {
                    return const Text(
                      'Error Picking Image',
                      textAlign: TextAlign.center,
                    );
                  } else {
                    return const Text(
                      'No Image Selected',
                      textAlign: TextAlign.center,
                    );
                  }
                },
              ),
            ]),
          ),
          Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: TextFormField(
                controller: _telefonoController,
                decoration: const InputDecoration(
                  hintText: 'Teléfono del Establecimiento',
                  border: const OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white,
                ),
              )),
          Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: TextFormField(
                controller: _mailController,
                decoration: const InputDecoration(
                  hintText: 'Correo Electrónico del Negocio:',
                  border: const OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white,
                ),
              )),
          Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: Row(children: <Widget>[
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: "Servicio de Domicilio",
                    style: TextStyle(
                        color: Color.fromRGBO(100, 100, 100, 1), fontSize: 18),
                  ),
                ),
                Checkbox(
                  value: domicilio,
                  onChanged: (bool value) {
                    setState(() {
                      domicilio = value;
                    });
                  },
                )
              ])),
          if (domicilio == true)
            Container(
                padding: const EdgeInsets.only(bottom: 20.0),
                child: TextFormField(
                  controller: _costoController,
                  decoration: const InputDecoration(
                    hintText: 'Costo Domicilio:',
                    border: const OutlineInputBorder(),
                    filled: true,
                    fillColor: Colors.white,
                  ),
                )),
          RaisedButton(
            color: Color.fromRGBO(193, 29, 27, 1),
            onPressed: () {
              setState(() {
                _toggleTab();
              });
            },
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Siguiente",
                style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1), fontSize: 18),
              ),
            ),
          )
        ]));
  }

  widgetMapa() {
    return ListView(
        physics: const NeverScrollableScrollPhysics(),
        children: <Widget>[
          if (lat == null)
            Container(
                margin: const EdgeInsets.only(
                    bottom: 80.0, top: 80.0, left: 120.0, right: 120.0),
                height: 100,
                child: CircularProgressIndicator(
                  value: null,
                  strokeWidth: 5.0,
                )),
          /*if (lat == null)
      Container(
      height: 500,
      child: FlutterMap(
            options: new MapOptions(
           center: new LatLng(18.052146, -76.826312), minZoom: 10.0),
            //center: location(), minZoom: 5.0),
            layers: [
              new TileLayerOptions(
                  urlTemplate:
                      "https://api.mapbox.com/styles/v1/pacho0522/ckdex357s5hnv1inz5r1kbbog/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoicGFjaG8wNTIyIiwiYSI6ImNrZGFybWt6eTA3NzIycHQ4YTlnYjRuanUifQ.qXv2hwJ7NKNrKZDU5kR4dQ",
                  additionalOptions: {
                    'accessToken':
                        'pk.eyJ1IjoicGFjaG8wNTIyIiwiYSI6ImNrZGV3Yzc3cTBiajkycW5hczhzaG00cWUifQ.TrdKX7gvxKd3ghC-_uw8aA',
                    'id': 'mapbox.mapbox-streets-v7'
                  })
            ])),*/
          if (lat != null)
            Container(
                height: 500,
                child: FlutterMap(
                    options: new MapOptions(
                      center: new LatLng(lat, lon),
                      minZoom: 10.0,
                      interactive: true,
                    ),
                    layers: [
                      new TileLayerOptions(
                          urlTemplate:
                              "https://api.mapbox.com/styles/v1/pacho0522/ckdex357s5hnv1inz5r1kbbog/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoicGFjaG8wNTIyIiwiYSI6ImNrZGFybWt6eTA3NzIycHQ4YTlnYjRuanUifQ.qXv2hwJ7NKNrKZDU5kR4dQ",
                          additionalOptions: {
                            'accessToken':
                                'pk.eyJ1IjoicGFjaG8wNTIyIiwiYSI6ImNrZGV3Yzc3cTBiajkycW5hczhzaG00cWUifQ.TrdKX7gvxKd3ghC-_uw8aA',
                            'id': 'mapbox.mapbox-streets-v7'
                          }),
                      MarkerLayerOptions(markers: [
                        Marker(
                            width: 45.0,
                            height: 45.0,
                            point: new LatLng(lat, lon),
                            builder: (context) => new Container(
                                  child: IconButton(
                                    icon: Icon(Icons.location_on),
                                    color: Colors.blue,
                                    iconSize: 30.0,
                                    onPressed: () {
                                      print('1');
                                    },
                                  ),
                                ))
                      ]),
                    ])),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    _toggleTabPrev();
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Volver",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
                if (lat != null && widget.status == "crear")
                  RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {
                      enviarInfo(
                          _nombresController.text,
                          _dirController.text,
                          _telefonoController.text,
                          domicilio,
                          _costoController.text,
                          _mailController.text,
                          dropdownValue,
                          _descripcionController.text);
                    },
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Crear",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  ),
                if (lat != null && widget.status == "editar")
                  RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {
                      editarInfo(
                          _nombresController.text,
                          _dirController.text,
                          _telefonoController.text,
                          domicilio,
                          _costoController.text,
                          _mailController.text,
                          dropdownValue,
                          _descripcionController.text);
                    },
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Editar",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  )
              ])
        ]);
  }

  widgetDescripcion() {
    return ListView(
        physics: const NeverScrollableScrollPhysics(),
        children: <Widget>[
          Container(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: Container(
                  height: 60.0,
                  padding: const EdgeInsets.only(left: 10.0, top: 5.0),
                  decoration: const BoxDecoration(
                    border: Border(
                      top: BorderSide(
                        width: 1,
                        color: Color.fromRGBO(120, 120, 120, 1),
                      ),
                      left: BorderSide(
                        width: 1,
                        color: Color.fromRGBO(120, 120, 120, 1),
                      ),
                      right: BorderSide(
                        width: 1,
                        color: Color.fromRGBO(120, 120, 120, 1),
                      ),
                      bottom: BorderSide(
                        width: 1,
                        color: Color.fromRGBO(120, 120, 120, 1),
                      ),
                    ),
                    borderRadius: BorderRadius.all(
                        Radius.circular(5.0) //         <--- border radius here
                        ),
                  ),
                  child: DropdownButton<String>(
                    isExpanded: true,
                    value: dropdownValue,
                    icon: Icon(Icons.arrow_downward),
                    iconSize: 30,
                    style: TextStyle(
                        color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
                    onChanged: (String newValue) {
                      setState(() {
                        dropdownValue = newValue;
                      });
                    },
                    items: <String>[
                      'Categoría',
                      'otros',
                      'Supermercados',
                      'Restaurantes',
                      'Panaderías',
                      'Minimercados',
                      'Licorerías-Cigarrerías',
                      'Droguerías',
                      'Comidas rápidas',
                      'Cafeterías',
                      'Tienda de Barrio'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ))),
          TextFormField(
            controller: _dirController,
            decoration: const InputDecoration(
              hintText: 'Dirección del Establecimiento',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          ),
          Container(
              padding: const EdgeInsets.only(bottom: 20.0, top: 20.0),
              child: TextFormField(
                maxLines: 10,
                controller: _descripcionController,
                decoration: const InputDecoration(
                  hintText: 'Descripción del Negocio',
                  border: const OutlineInputBorder(),
                  filled: true,
                  fillColor: Colors.white,
                ),
              )),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    _toggleTabPrev();
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Volver",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                ),
                RaisedButton(
                  color: Color.fromRGBO(193, 29, 27, 1),
                  onPressed: () {
                    _toggleTab2();
                  },
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Siguiente",
                      style: TextStyle(
                          color: Color.fromRGBO(255, 255, 255, 1),
                          fontSize: 18),
                    ),
                  ),
                )
              ])
        ]);
  }

  _getLocation() async {
    try {
      geolocator
          .placemarkFromCoordinates(
              _currentPosition.latitude, _currentPosition.longitude)
          .then((List<Placemark> p) {
        setState(() {
          place = p[0];
          geolocator
              .placemarkFromAddress(_dirController.text + place.locality)
              .then((List<Placemark> placemark) {
            setState(() {
              lat = placemark[0].position.latitude;
              lon = placemark[0].position.longitude;
            });

            print(placemark[0].position);
          }).catchError((e) {
            print(e);
          });
        });
      }).catchError((e) {
        print(e);
      });
    } catch (e) {
      print(e);
    }

    print(lat);
  }

  _getCurrentLocation() async {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
    }).catchError((e) {
      print(e);
    });
  }

  enviarInfo(nombre, direccion, telefono, domicilio, costo, email, categoria,
      descripcion) async {
    if (categoria == 'otros') {
      categoria = "10";
    } else if (categoria == 'Restaurantes') {
      categoria = "9";
    } else if (categoria == 'Cafeterías') {
      categoria = "8";
    } else if (categoria == 'Licorerías-Cigarrerías') {
      categoria = "7";
    } else if (categoria == 'Droguerías') {
      categoria = "6";
    } else if (categoria == 'Supermercados') {
      categoria = "5";
    } else if (categoria == 'Minimercados') {
      categoria = "4";
    } else if (categoria == 'Panaderías') {
      categoria = "3";
    } else if (categoria == 'Comidas rápidas') {
      categoria = "2";
    } else if (categoria == 'Tienda de Barrio') {
      categoria = "1";
    }

    final mimeTypeData =
        lookupMimeType(tmpFile.path, headerBytes: [0xFF, 0xD8]).split('/');
    var uri = Uri.parse('http://50.116.46.197:8000/api/1.0/negocio/create/');
    var request = new http.MultipartRequest("POST", uri);

    request.fields['nombre'] = nombre;
    final file = await http.MultipartFile.fromPath(
      'fachada',
      tmpFile.path,
      contentType: MediaType(mimeTypeData[0], mimeTypeData[1]),
    );
    request.fields['direccion'] = direccion;
    request.fields['telefono'] = telefono;
    request.fields['domicilio'] = domicilio.toString();
    request.fields['costo'] = costo;
    request.fields['email_negocio'] = email;
    request.fields['categoria'] = categoria;
    request.fields['descripcion'] = descripcion;
    request.fields['lon'] = lat.toString();
    request.fields['lat'] = lon.toString();

    request.files.add(file);
    request.headers['Authorization'] = 'Token ' + widget.token.toString();

    var streamedResponse = await request.send();
    var response = await http.Response.fromStream(streamedResponse);
    print(response.body);

    if (response.statusCode == 201) {
      crearNegocio();
    }
  }

  Future<void> crearNegocio() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Negocio Creado'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Tu negocio se creo correctamente'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (BuildContext context) => HomePageLogin(
                            token: widget.token, idUsuario: widget.idUsuario)),
                    (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }

  editarInfo(nombre, direccion, telefono, domicilio, costo, email, categoria,
      descripcion) async {
    if (nombre == "") {
      nombre = widget.infoNegocio["nombre"].toString();
    }

    if (direccion == "") {
      direccion = widget.infoNegocio["direccion"];
      lon = widget.infoNegocio["lat"];
      lat = widget.infoNegocio["lon"];
    }
    if (telefono == "") {
      telefono = widget.infoNegocio["telefono"];
    }
    if (domicilio == false) {
      domicilio = widget.infoNegocio["domicilio"];
    }
    if (costo == "") {
      costo = widget.infoNegocio["costo"];
    }
    if (email == "") {
      email = widget.infoNegocio["email_negocio"];
    }
    if (categoria == "Categoría") {
      categoria = widget.infoNegocio["categoria"].toString();
    }
    if (descripcion == "") {
      descripcion = widget.infoNegocio["descripcion"];
    }

    if (tmpFile == null) {
      var rng = new Random();
      Directory tempDir = await getTemporaryDirectory();
      String tempPath = tempDir.path;
      File file =
          new File('$tempPath' + (rng.nextInt(100)).toString() + '.png');
      http.Response response = await http.get(widget.infoNegocio["fachada"]);
      await file.writeAsBytes(response.bodyBytes);
      tmpFile = file;
    }

    if (categoria == 'otros') {
      categoria = "10";
    } else if (categoria == 'Restaurantes') {
      categoria = "9";
    } else if (categoria == 'Cafeterías') {
      categoria = "8";
    } else if (categoria == 'Licorerías-Cigarrerías') {
      categoria = "7";
    } else if (categoria == 'Droguerías') {
      categoria = "6";
    } else if (categoria == 'Supermercados') {
      categoria = "5";
    } else if (categoria == 'Minimercados') {
      categoria = "4";
    } else if (categoria == 'Panaderías') {
      categoria = "3";
    } else if (categoria == 'Comidas rápidas') {
      categoria = "2";
    } else if (categoria == 'Tienda de Barrio') {
      categoria = "1";
    }

    final mimeTypeData =
        lookupMimeType(tmpFile.path, headerBytes: [0xFF, 0xD8]).split('/');
    var uri = Uri.parse('http://50.116.46.197:8000/api/1.0/negocio/' +
        widget.infoNegocio["id"].toString() +
        '/');
    var request = new http.MultipartRequest("PUT", uri);

    request.fields['nombre'] = nombre;
    final file = await http.MultipartFile.fromPath(
      'fachada',
      tmpFile.path,
      contentType: MediaType(mimeTypeData[0], mimeTypeData[1]),
    );
    request.fields['direccion'] = direccion;
    request.fields['telefono'] = telefono;
    request.fields['domicilio'] = domicilio.toString();
    request.fields['costo'] = costo;
    request.fields['email_negocio'] = email;
    request.fields['categoria'] = categoria;
    request.fields['descripcion'] = descripcion;
    request.fields['lon'] = lat.toString();
    request.fields['lat'] = lon.toString();

    request.files.add(file);
    request.headers['Authorization'] = 'Token ' + widget.token.toString();

    var streamedResponse = await request.send();
    var response = await http.Response.fromStream(streamedResponse);
    print(response.body);

    if (response.statusCode == 200) {
      editarNegocio();
    }
  }

  Future<void> editarNegocio() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Negocio Editado'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Tu negocio se edito correctamente'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (BuildContext context) => HomePageLogin(
                            token: widget.token, idUsuario: widget.idUsuario)),
                    (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }
}
