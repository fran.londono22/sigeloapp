import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:siguelo_app/crearProductos.dart';
import 'package:siguelo_app/home.dart';

class ProductosPage extends StatefulWidget {
  ProductosPage({
    Key key,
    this.title,
    this.productos,
    this.token,
    this.idUsuario
    
  }) : super(key: key);

  final String title;
  var productos;
  String token;
  var idUsuario;
  @override
  _ProductosPage createState() => _ProductosPage();
}

class _ProductosPage extends State<ProductosPage> {
//List pedidos = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child:  Container(
        child: Column(
          children: <Widget>[
            widgetProductos(),
          ],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }


  widgetProductos(){
    if (widget.productos[1]["productos"].isNotEmpty) {
      return Expanded(
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: widget.productos[1]["productos"].length,
          padding: const EdgeInsets.all(8),
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return Card(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                      color: Color.fromRGBO(54, 185, 204, 1),
                      width: MediaQuery.of(context).size.width,
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: widget.productos[1]["productos"][index]["nombre"],
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 20),
                        ),
                      )),
                  Image.network(
                    "http://" + widget.productos[1]["productos"][index]["foto"],
                  ),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: "Precio: ",
                      style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                      children: <TextSpan>[
                        TextSpan(
                            text: "\$" +
                                widget.productos[1]["productos"][index]["precio"]
                                    .toString(),
                            style: TextStyle(fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.only(
                          left: 20.0, top: 20.0, right: 20.0),
                      child: RaisedButton(
                        color: Color.fromRGBO(193, 29, 27, 1),
                        onPressed: () {

                          editarProducto(widget.productos[1]["productos"][index]);

                         
                        },
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: "Editar",
                            style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontSize: 18),
                          ),
                        ),
                      )),
                      Container(
                      width: MediaQuery.of(context).size.width,
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0),
                      child: RaisedButton(
                        color: Color.fromRGBO(193, 29, 27, 1),
                        onPressed: () async {
                          
                          eliminarProducto(widget.productos[1]["productos"][index]["id"]);

                         
                        },
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: "Eliminar",
                            style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontSize: 18),
                          ),
                        ),
                      )),
                ],
              ),
            );
          }));
    //}
    } else {
      return Expanded(
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "No tiene productos creados!",
            style: TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
          ),
        ),
      );
    }

  }

  Future<void> eliminarProducto(id) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Eliminar Producto'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('¿Desea eliminar el producto?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Si'),
              onPressed: () {
                
              eliminar(id);
              },
            ),
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  eliminar(id)async{

    final response = await http.delete(
        'http://50.116.46.197:8000/api/1.0/producto/' + id.toString() + '/',
        headers: {'Authorization': 'Token ' + widget.token});

        if(response.statusCode == 204){

    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePageLogin( token: widget.token, idUsuario: widget.idUsuario)), (Route<dynamic> route) => false);
        }

  }

  editarProducto(producto){

    Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CrearProductosPage(
                                        idUsuario: widget.idUsuario,
                                        token: widget.token,
                                        idNegocio: null,
                                        producto: producto,
                                        status: "Editar")),
                              );

  }

 
}