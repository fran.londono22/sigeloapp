import 'package:flutter/material.dart';
import 'carritoDeCompras.dart';
import 'package:flutter_clean_calendar/flutter_clean_calendar.dart';
import 'package:intl/intl.dart';
import 'package:date_util/date_util.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class AgendaPage extends StatefulWidget {
  AgendaPage(
      {Key key,
      this.username,
      this.password,
      this.token,
      this.servicioInfo,
      this.servicioHorario,
      this.idServicio})
      : super(key: key);

  String username;
  String password;
  String token;
  var servicioInfo;
  var servicioHorario;
  var idServicio;

  @override
  _AgendaPage createState() => _AgendaPage();
}

class _AgendaPage extends State<AgendaPage> {
  int dia;
  int mes;
  int year;
  int dias;
  var dateUtility = new DateUtil();
  String diaJson;

  List disponible = [];
  List horarios = [];

  double _progress = 0;
  
  
  startTimer() {
    new Timer.periodic(
      Duration(seconds: 1),
      (Timer timer) => setState(
        () {
          if (_progress == 1) {
            timer.cancel();
          } else {
            _progress += 0.25;
          }
        },
      ),
    );
  }

  void _handleNewDate(date) {
     
    //print(date.toString().substring(0, 10));
    horarios = [];
    consultarDisponibildiad(date.toString().substring(0, 10));
    setState(() {
      _selectedDay = date;
      _selectedEvents = _events[_selectedDay] ?? [];      
    _progress = 0;
    });
  }

  List _selectedEvents;
  DateTime _selectedDay;

  final Map<DateTime, List> _events = {
    /*DateTime(2020, 8, 9): [
      {'name': 'Event A', 'isDone': true},
      {'name': 'Event B', 'isDone': true},
    ],
    DateTime(2020, 8, 10): [
      {'name': 'Event A', 'isDone': true},
      {'name': 'Event B', 'isDone': true},
    ],*/
  };

  @override
  void initState() {
    super.initState();
    _selectedEvents = _events[_selectedDay] ?? [];
    dia = new DateTime.now().day;
    mes = new DateTime.now().month;
    year = new DateTime.now().year;
    dias = dateUtility.daysInMonth(mes, year);

//print(widget.servicioHorario.length);

    for (var j = 0; j < widget.servicioHorario.length; j++) {
//print(j);

      if (widget.servicioHorario[j]["day"] == "lunes") {
        diaJson = "Monday";
      }
      if (widget.servicioHorario[j]["day"] == "martes") {
        diaJson = "Thursday";
      }
      if (widget.servicioHorario[j]["day"] == "miercoles") {
        diaJson = "Wednesday";
      }
      if (widget.servicioHorario[j]["day"] == "jueves") {
        diaJson = "Tuesday";
      }
      if (widget.servicioHorario[j]["day"] == "viernes") {
        diaJson = "Friday";
      }

      for (int iYear = year; iYear <= 2030; iYear++) {
        for (int iMes = 1; iMes <= 12; iMes++) {
          for (int iDia = 1; iDia <= dias; iDia++) {
            var diaComparar = new DateTime.utc(iYear, iMes, iDia);

            if (new DateFormat('EEEE').format(diaComparar) == diaJson) {
              var horario;
              if (widget.servicioHorario[j]["from_1"].length == 4) {
                horario = new DateTime.utc(
                    iYear,
                    iMes,
                    iDia,
                    int.parse(
                        widget.servicioHorario[j]["from_1"].substring(0, 1)),
                    int.parse(
                        widget.servicioHorario[j]["from_1"].substring(2, 4)));
              }

              if (widget.servicioHorario[j]["from_1"].length == 5) {
                horario = new DateTime.utc(
                    iYear,
                    iMes,
                    iDia,
                    int.parse(
                        widget.servicioHorario[j]["from_1"].substring(0, 2)),
                    int.parse(
                        widget.servicioHorario[j]["from_1"].substring(3, 5)));
                // print(horario.toString().substring(12, 16));
              }

              List franjas = [];

              for (var h = horario.hour;
                  h <
                      int.parse(
                          widget.servicioHorario[j]["to_1"].substring(0, 2));) {
                franjas.add({
                  'Dia': iYear.toString() +
                      "-" +
                      iMes.toString() +
                      "-" +
                      iDia.toString(),
                  'Horario': horario.toString().substring(11, 16) +
                      " - " +
                      horario
                          .add(new Duration(
                              minutes: widget.servicioInfo["time"]))
                          .toString()
                          .substring(11, 16),
                  'isDone': false
                });

                horario = horario
                    .add(new Duration(minutes: widget.servicioInfo["time"]));
                h = horario.hour;
              }
              _events.putIfAbsent(DateTime(iYear, iMes, iDia), () => franjas);

              if (widget.servicioHorario[j]["from_2"].length == 4) {
                horario = new DateTime.utc(
                    iYear,
                    iMes,
                    iDia,
                    int.parse(
                        widget.servicioHorario[j]["from_2"].substring(0, 1)),
                    int.parse(
                        widget.servicioHorario[j]["from_2"].substring(2, 4)));
              }

              if (widget.servicioHorario[j]["from_2"].length == 5) {
                horario = new DateTime.utc(
                    iYear,
                    iMes,
                    iDia,
                    int.parse(
                        widget.servicioHorario[j]["from_2"].substring(0, 2)),
                    int.parse(
                        widget.servicioHorario[j]["from_2"].substring(3, 5)));
                // print(horario.toString().substring(12, 16));
              }

              List franjas2 = [];

              for (var h = horario.hour;
                  h <
                      int.parse(
                          widget.servicioHorario[j]["to_2"].substring(0, 2));) {
                franjas.add({
                  'Dia': iYear.toString() +
                      "-" +
                      iMes.toString() +
                      "-" +
                      iDia.toString(),
                  'Horario': horario.toString().substring(11, 16) +
                      " - " +
                      horario
                          .add(new Duration(
                              minutes: widget.servicioInfo["time"]))
                          .toString()
                          .substring(11, 16),
                  'isDone': false
                });

                horario = horario
                    .add(new Duration(minutes: widget.servicioInfo["time"]));
                h = horario.hour;
              }
              _events.putIfAbsent(DateTime(iYear, iMes, iDia), () => franjas2);
            }
          }
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: Column(
          children: <Widget>[wigdetAgenda(), _buildEventList()],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[widgetLogo(), widgetCarrito()]));
  }

  widgetCarrito() {
    return IconButton(
      icon: Icon(Icons.shopping_cart),
      color: Color.fromRGBO(193, 29, 27, 1),
      iconSize: 30.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CarritoPage(
                  username: widget.username,
                  password: widget.password,
                  token: widget.token)),
        );
      },
    );
  }

  wigdetAgenda() {
    return Container(
      child: Calendar(
        startOnMonday: true,
        weekDays: ["L", "M", "M", "J", "V", "S", "D"],
        events: _events,
        /* onRangeSelected: (range) =>
                    print("Range is ${range.from}, ${range.to}"),*/
        onDateSelected: (date) => _handleNewDate(date),
        isExpandable: true,
        eventDoneColor: Colors.green,
        selectedColor: Colors.pink,
        todayColor: Colors.yellow,
        eventColor: Colors.grey,
        dayOfWeekStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w800, fontSize: 11),
      ),
    );
  }

  Widget _buildEventList() {
   
    startTimer();
    if (_progress != 1) {
      return Container(
          height: 20,
          child: CircularProgressIndicator(
            value: null,
            strokeWidth: 5.0,
          ));
    } else {
    return Expanded(
      child: ListView.builder(
        itemBuilder: (BuildContext context, int index) => Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(width: 1.5, color: Colors.black12),
            ),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 4.0),
          child: ListTile(
            title: mostrarFranja(index),
            onTap: () {
              _agendar(_selectedEvents[index], index);
            },
          ),
        ),
        itemCount: horarios.length,
      ),
    );
  }
  }
  
  mostrarFranja(index) {
    /*if(horarios[index].toString().substring(14, 19) == "Libre" ){
    print(horarios[index].toString().substring(14, 19));
    }*/

    
      if (horarios[index].toString().substring(14, 16) == "Si") {
        return Text(
          horarios[index].toString(),
          style: TextStyle(color: Colors.red),
        );
      } else {
        return Text(horarios[index].toString());
      }
  }

  consultarDisponibildiad(dia) async {
    disponible = [];
    final response1 = await http.get(
        'http://50.116.46.197:8000/api/1.0/reservacion/list/' +
            dia.toString() +
            '/' +
            widget.idServicio.toString() +
            '/',
        headers: {
          'Authorization': 'Token ' + widget.token,
          'Content-type': 'application/json'
        });
    var data1 = utf8.decode(response1.bodyBytes);
    var servicioHorario = jsonDecode(data1);
    for (var i = 0; i < servicioHorario.length; i++) {
      disponible.add(servicioHorario[i]["from_t"].toString().substring(0, 5));
    }

    for (var i = 0; i < _selectedEvents.length; i++) {
      Map<String, dynamic> data = {
        'fecha': dia.toString(),
        'franja': _selectedEvents[i]['Horario'].substring(0, 5),
      };

      final disponibilidad = await http.post(
          'http://50.116.46.197:8000/api/1.0/reservacion/queues/' +
              widget.idServicio.toString() +
              '/',
          body: data,
          headers: {'Authorization': 'Token ' + widget.token});

      var jsonResponseDisponibilidad = json.decode(disponibilidad.body);
      print(jsonResponseDisponibilidad["mensaje"]);

      if (jsonResponseDisponibilidad["mensaje"] == "OK") {
        setState(() {
          horarios.add(_selectedEvents[i]['Horario'] +
              " Tomados " +
              jsonResponseDisponibilidad["reservaciones"] +
              " de " +
              jsonResponseDisponibilidad["maximo_reservaciones"]);
        });
      }/* else {
        setState(() {
          horarios.add(_selectedEvents[i]['Horario'] + " Sin cupo");
        });
      }*/
    }
  }

  Future<void> _agendar(info, index) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Agendar Servicio'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('¿Desea agendar el servicio para el día ?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Si'),
              onPressed: () {
                enviarInfo(info, index);
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  enviarInfo(info, index) async {
    Map<String, dynamic> data = {
      'day': info["Dia"].toString(),
      'from_t': info["Horario"].toString().substring(0, 5),
      'estado': "false",
      'confirmacion_llegada': "false",
      'pendiente': "false"
    };

    var response = await http.post(
        'http://50.116.46.197:8000/api/1.0/reservation/' +
            widget.idServicio.toString() +
            '/',
        body: data,
        headers: {'Authorization': "Token " + widget.token});

    var jsonResponse = json.decode(response.body);
    print(jsonResponse);
    if (response.statusCode == 201) {
      setState(() {
        //Poiner rojo _events.remove(index);
      });
    }
  }
}
