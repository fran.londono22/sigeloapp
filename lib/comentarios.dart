import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ComentariosPage extends StatefulWidget {
  ComentariosPage({
    Key key,
    this.title,
    this.nombreNegocio,
    this.idNegocio,
    this.tipo
  }) : super(key: key);

  final String title;
  var nombreNegocio;
  var idNegocio;
  var tipo;

  @override
  _ComentariosPage createState() => _ComentariosPage();
}


class ComentarioData {
  int id;  
  String negocio;
  String comentario;
  int puntuacion;
  String usuario;

  ComentarioData(id, negocio, comentario, puntuacion, usuario) {
    this.id = id;
    this.negocio = negocio;
    this.comentario = comentario;
    this.puntuacion = puntuacion;
    this.usuario = usuario;
  }
}

class _ComentariosPage extends State<ComentariosPage> {
  var idNegocio = [];
  var nombreNegocio = [];
  List<ComentarioData> comentario = [];

  @override
  void initState() {
    super.initState();
    idNegocio = widget.idNegocio;
    nombreNegocio = widget.nombreNegocio;
    if(widget.tipo == "negocio"){
    setState(() {      
    abrirComentarios();
    });
    }
    if(widget.tipo == "tienda"){
    setState(() {      
    abrirComentariosTienda();
    });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  abrirComentarios() async {
    final prefs = await SharedPreferences.getInstance();
    final myStringList = prefs.getStringList('datosInicio') ?? [];

    for (var j = 0; j < idNegocio.length; j++) {
      final response1 = await http.get(
          'http://50.116.46.197:8000/api/1.0/negocio/calificacion/' +
              idNegocio[j].toString() +
              '/',
          headers: {'Authorization': "Token " + myStringList[0],
          'Content-type': 'application/json, charset: "utf-8'});

      var data1 = jsonDecode(response1.body);
      if(data1["calificaciones"] != null){
        
          
        
        for (var i = 0; i < data1["calificaciones"].length; i++){
          setState(() {
            
        comentario.add(ComentarioData(data1["calificaciones"][i]["id"], nombreNegocio[j] ,data1["calificaciones"][i]["comentario"],
        data1["calificaciones"][i]["puntuacion"], data1["calificaciones"][i]["usuario"],));
         print(comentario[i].negocio);
         
          });
        }
        print(comentario.length);
      }
     
    }
  }

  abrirComentariosTienda() async {
    final prefs = await SharedPreferences.getInstance();
    final myStringList = prefs.getStringList('datosInicio') ?? [];

    for (var j = 0; j < idNegocio.length; j++) {
      final response1 = await http.get(
          'http://50.116.46.197:8000/api/1.0/servicios/calificacion/' +
              idNegocio[j].toString() +
              '/',
          headers: {'Authorization': "Token " + myStringList[0],
          'Content-type': 'application/json, charset: "utf-8'});

      var data1 = jsonDecode(response1.body);
      if(data1["calificaciones"] != null){
        
          
        
        for (var i = 0; i < data1["calificaciones"].length; i++){
          setState(() {
            
        comentario.add(ComentarioData(data1["calificaciones"][i]["id"], nombreNegocio[j] ,data1["calificaciones"][i]["comentario"],
        data1["calificaciones"][i]["puntuacion"], data1["calificaciones"][i]["usuario"],));
         print(comentario[i].negocio);
         
          });
        }
        print(comentario.length);
      }
     
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: ListView(
          children: <Widget>[
            widgetPedidos(),
          ],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }

  widgetPedidos() {
    return Expanded(
        child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            scrollDirection: Axis.vertical,
            itemCount: comentario.length,
            padding: const EdgeInsets.all(8),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[      
                    Container(
                        color: Color.fromRGBO(54, 185, 204, 1),
                        width: MediaQuery.of(context).size.width,
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: comentario[index].negocio,
                            style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontSize: 20),
                          ),
                        )),              
                       RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          text: comentario[index].comentario,
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          text: "Hecho por - " + comentario[index].usuario,
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          text: comentario[index].puntuacion.toString(),
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                        ),
                      ),
                      Container(
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0),
                  bottomRight: Radius.circular(10.0))),
          child: FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              "Eliminar",
              textScaleFactor: 1.3,
            ),
          ),
        ),
                  ],
                ),
              );
            }));
    }
}

