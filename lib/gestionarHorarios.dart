import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:http_parser/http_parser.dart';
import 'dart:io';

class GestionHorarioPage extends StatefulWidget {
  GestionHorarioPage(
      {Key key, this.idUsuario, this.token, this.idNegocio, this.status})
      : super(key: key);

  var idUsuario;
  var idNegocio;
  var producto;
  String token;
  String status;
  @override
  _GestionHorarioPage createState() => _GestionHorarioPage();
}

class Horario {
  String dia;
  String hora;
  String minuto;

  Horario(dia, hora, minuto) {
    this.dia = dia;
    this.hora = hora;
    this.minuto = minuto;
  }
}

class _GestionHorarioPage extends State<GestionHorarioPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  String status = '';
  List<bool> isSelected = [false, false, false, false, false, false, false];
  TimeOfDay time = TimeOfDay.now();
  TimeOfDay tiempo;

  List<Horario> listHorario = [];

  var lunesDesde1 = "";
  var lunesHasta1 = "";
  var lunesDesde2 = "";
  var lunesHasta2 = "";

   var martesDesde1 = "";
  var martesHasta1 = "";
  var martesDesde2 = "";
  var martesHasta2 = "";

   var miercolesDesde1 = "";
  var miercolesHasta1 = "";
  var miercolesDesde2 = "";
  var miercolesHasta2 = "";

   var juevesDesde1 = "";
  var juevesHasta1 = "";
  var juevesDesde2 = "";
  var juevesHasta2 = "";

   var viernesDesde1 = "";
  var viernesHasta1 = "";
  var viernesDesde2 = "";
  var viernesHasta2 = "";

   var sabadoDesde1 = "";
  var sabadoHasta1 = "";
  var sabadoDesde2 = "";
  var sabadoHasta2 = "";

   var domingoDesde1 = "";
  var domingoHasta1 = "";
  var domingoDesde2 = "";
  var domingoHasta2 = "";


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: ListView(
          children: <Widget>[
            widgetTitulo(),
            widgetDias(),
            widgetHoras(),
          ],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }

  Widget widgetTituloMobile() {
    return Container(
        padding: const EdgeInsets.only(bottom: 20.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Crear Cola",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 40),
          ),
        ));
  }

  widgetTitulo(){
    return Container(
      padding: const EdgeInsets.only(top: 20.0), 
      child: Center(
      child: 
      Text(
                      "Seleccione los días que se presta el servicio",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, ),
                    )));
  }

  widgetDias() {
    return Container(
        padding: const EdgeInsets.only(top: 20.0),
        alignment: Alignment(0, 0),
        child: ToggleButtons(
          children: <Widget>[
            Text("Lun"),
            Text("Mar"),
            Text("Mie"),
            Text("Jue"),
            Text("Vie"),
            Text("Sab"),
            Text("Dom"),
          ],
          onPressed: (int index) {
            setState(() {
              isSelected[index] = !isSelected[index];
              print(isSelected[0]);
            });
          },
          isSelected: isSelected,
        ));
  }

  widgetHoras() {
    return Column(
      children: <Widget>[
        if (isSelected[0] == true)
        widgetLunes(),
        if (isSelected[1] == true)
        widgetMartes(),
        if (isSelected[2] == true)
        widgetMiercoles(),
        if (isSelected[3] == true)
        widgetJueves(),
        if (isSelected[4] == true)
        widgetViernes(),
        if (isSelected[5] == true)
        widgetSabado(),
        if (isSelected[6] == true)
        widgetDomingo()
      ]);}

        widgetLunes(){
          return Container(
              padding:
                  const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.only(left: 40.0, right: 40.0),
                      color: Color.fromRGBO(90, 92, 105, 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Horario 1",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                          Container(
                            child: Text(
                              "LUNES",
                              style: TextStyle(
                                  fontSize: 15.0, fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                          Container(
                            child: Text(
                              "Horario 2",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                        ],
                      )),
                  Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "LunesDesde1");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(lunesDesde1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "LunesDesde2");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(lunesDesde2),
                    ],
                  )),
                  Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "LunesHasta1");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(lunesHasta1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "LunesHasta2");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(lunesHasta2),
                    ],
                  )),
                  FlatButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    textColor: Colors.white,
                    padding: EdgeInsets.only(left: 50.0, right: 50.0 ),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      /*...*/
                    },
                    child: Text(
                      "Actualizar Lunes",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  )
                ],
              ));}


        widgetMartes(){
          return Container(
              padding:
                  const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.only(left: 40.0, right: 40.0),
                      color: Color.fromRGBO(90, 92, 105, 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Horario 1",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                          Container(
                            child: Text(
                              "MARTES",
                              style: TextStyle(
                                  fontSize: 15.0, fontWeight: FontWeight.bold,
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                          Container(
                            child: Text(
                              "Horario 2",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                        ],
                      )),
                  Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "MartesDesde1");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(martesDesde1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "MartesDesde2");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(martesDesde2),
                    ],
                  )),
                  
                  Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "MartesHasta1");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(martesHasta1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "MartesHasta2");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(martesHasta2),
                    ],
                  )),
                  FlatButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    textColor: Colors.white,
                    padding: EdgeInsets.only(left: 50.0, right: 50.0 ),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      /*...*/
                    },
                    child: Text(
                      "Actualizar Martes",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  )
                ],
              ));
        }

        widgetMiercoles(){
          Container(
              padding:
                  const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.only(left: 40.0, right: 40.0),
                      color: Color.fromRGBO(90, 92, 105, 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Horario 1",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                          Container(
                            child: Text(
                              "Horario 2",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                        ],
                      )),
                  Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "MiercolesDesde1");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(miercolesDesde1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "MiercolesDesde2");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(miercolesDesde2),
                    ],
                  )),
                   Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "MiercolesHasta1");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(miercolesHasta1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "MiercolesHasta2");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(miercolesHasta2),
                    ],
                  )),
                  FlatButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    textColor: Colors.white,
                    padding: EdgeInsets.only(left: 50.0, right: 50.0 ),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      /*...*/
                    },
                    child: Text(
                      "Actualizar Miercoles",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  )
                ],
              ));}

        widgetJueves(){
          Container(
              padding:
                  const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.only(left: 40.0, right: 40.0),
                      color: Color.fromRGBO(90, 92, 105, 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Horario 1",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                          Container(
                            child: Text(
                              "Horario 2",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                        ],
                      )),
                  Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "JuevesDesde1");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(juevesDesde1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "JuevesDesde2");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(juevesDesde2),
                    ],
                  )),
                   Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "JuevesHasta1");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(juevesHasta1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "JuevesHasta2");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(juevesHasta2),
                    ],
                  )),
                  FlatButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    textColor: Colors.white,
                    padding: EdgeInsets.only(left: 50.0, right: 50.0 ),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      /*...*/
                    },
                    child: Text(
                      "Actualizar Jueves",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  )
                ],
              ));
        }

        widgetViernes(){
          Container(
              padding:
                  const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.only(left: 40.0, right: 40.0),
                      color: Color.fromRGBO(90, 92, 105, 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Horario 1",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                          Container(
                            child: Text(
                              "Horario 2",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                        ],
                      )),
                  Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "ViernesDesde1");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(viernesDesde1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "ViernesDesde2");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(viernesDesde2),
                    ],
                  )),
                   Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "ViernesHasta1");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(viernesHasta1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "ViernesHasta2");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(viernesHasta2),
                    ],
                  )),
                  FlatButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    textColor: Colors.white,
                    padding: EdgeInsets.only(left: 50.0, right: 50.0 ),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      /*...*/
                    },
                    child: Text(
                      "Actualizar Viernes",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  )
                ],
              ));}
        
        widgetSabado(){
          Container(
              padding:
                  const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.only(left: 40.0, right: 40.0),
                      color: Color.fromRGBO(90, 92, 105, 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Horario 1",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                          Container(
                            child: Text(
                              "Horario 2",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                        ],
                      )),
                  Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "SabadoDesde1");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(sabadoDesde1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "SabadoDesde2");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(sabadoDesde2),
                    ],
                  )),
                   Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "SabadoHasta1");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(sabadoHasta1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "SabadoHasta2");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(sabadoHasta2),
                    ],
                  )),
                  FlatButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    textColor: Colors.white,
                    padding: EdgeInsets.only(left: 50.0, right: 50.0 ),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      /*...*/
                    },
                    child: Text(
                      "Actualizar Sabado",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  )
                ],
              ));}

        widgetDomingo(){
         return Container(
              padding:
                  const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: const EdgeInsets.only(left: 40.0, right: 40.0),
                      color: Color.fromRGBO(90, 92, 105, 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Horario 1",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                          Container(
                            child: Text(
                              "Horario 2",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1)),
                            ),
                          ),
                        ],
                      )),
                  Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "DomingoDesde1");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(domingoDesde1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "DomingoDesde2");
                          }),
                      Container(
                        child: Text("Desde:"),
                      ),
                      Text(domingoDesde2),
                    ],
                  )),
                   Center(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "DomingoHasta1");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(domingoHasta1),
                      Text("|"),
                      IconButton(
                          icon: Icon(Icons.alarm),
                          onPressed: () {
                            timePicker(context, "DomingoHasta2");
                          }),
                      Container(
                        child: Text("Hasta:"),
                      ),
                      Text(domingoHasta2),
                    ],
                  )),
                  FlatButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    textColor: Colors.white,
                    padding: EdgeInsets.only(left: 50.0, right: 50.0 ),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      /*...*/
                    },
                    child: Text(
                      "Actualizar Domingo",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  )
                ],
              ));}


  Future<Null> timePicker(BuildContext context, dia) async {
    tiempo = await showTimePicker(
      context: context,
      initialTime: time,
    );

    if( tiempo.hour != null)
    setState(() {
      //Ojo que se estan creando y no eliminando cuando se editan!!!

      listHorario
          .add(Horario(dia, tiempo.hour.toString(), tiempo.minute.toString()));
      for (var i = 0; i < listHorario.length; i++) {

        if (listHorario[i].dia == "LunesDesde1") {
          lunesDesde1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "LunesHasta1") {
          lunesHasta1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "LunesDesde2") {
          lunesDesde2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "LunesHasta2") {
          lunesHasta2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }


        if (listHorario[i].dia == "MartesDesde1") {
          martesDesde1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "MartesHasta1") {
          martesHasta1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "MartesDesde2") {
          martesDesde2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "MartesHasta2") {
          martesHasta2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }



        if (listHorario[i].dia == "MiercolesDesde1") {
          miercolesDesde1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "MiercolesHasta1") {
          miercolesHasta1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "MiercolesDesde2") {
          miercolesDesde2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "MiercolesHasta2") {
          miercolesHasta2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }


        if (listHorario[i].dia == "JuevesDesde1") {
          juevesDesde1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "JuevesHasta1") {
          juevesHasta1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "JuevesDesde2") {
          juevesDesde2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "JuevesHasta2") {
          juevesHasta2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }


        if (listHorario[i].dia == "ViernesDesde1") {
          viernesDesde1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "ViernesHasta1") {
          viernesHasta1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "ViernesDesde2") {
          viernesDesde2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "ViernesHasta2") {
          viernesHasta2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }



        if (listHorario[i].dia == "SabadoDesde1") {
          sabadoDesde1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "SabadoHasta1") {
          sabadoHasta1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "SabadoDesde2") {
          sabadoDesde2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "SabadoHasta2") {
          sabadoHasta2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }



        if (listHorario[i].dia == "DomingoDesde1") {
          domingoDesde1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "DomingoHasta1") {
          domingoHasta1 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
         if (listHorario[i].dia == "DomingoDesde2") {
          domingoDesde2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
        if (listHorario[i].dia == "DomingoHasta2") {
          domingoHasta2 = listHorario[i].hora.toString() +
              ":" +
              listHorario[i].minuto.toString();
        }
      }
      print(listHorario.length);
    });
  }
}
