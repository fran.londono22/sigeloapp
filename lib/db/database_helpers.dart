import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

// database table and column names
final String tableProductos = 'productos';
final String columnId = '_id';
final String columnIdProducto = 'idProducto';
final String columnNombre = 'nombre';
final String columnPrecio = 'precio';
final String columnFoto = 'foto';
final String columnCantidad = 'cantidad';
final String columnTotal = 'total';

// data model class
class Producto {
  int id;
  int idProducto;
  String nombre;
  int precio;
  String foto;
  int cantidad;
  int total;

  Producto();

  // convenience constructor to create a Word object
  Producto.fromMap(Map<String, dynamic> map) {
    id = map[columnId];
    idProducto = map[columnIdProducto];
    nombre = map[columnNombre];
    precio = map[columnPrecio];
    foto = map[columnFoto];
    cantidad = map[columnCantidad];
    total = map[columnTotal];
  }

  // convenience method to create a Map from this Word object
  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{columnIdProducto: idProducto, columnNombre: nombre, columnPrecio: precio, columnFoto: foto, columnCantidad: cantidad, columnTotal: total};
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }
}

// singleton class to manage the database
class DatabaseHelper {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "MyDatabase.db";
  // Increment this version when you need to change the schema.
  static final _databaseVersion = 1;

  // Make this a singleton class.
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // Only allow a single open connection to the database.
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await _initDatabase();
    return _database;
  }

  // open the database
  _initDatabase() async {
    // The path_provider plugin gets the right directory for Android or iOS.
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    // Open the database. Can also add an onUpdate callback parameter.
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  _deleteDatabase() async{
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    await deleteDatabase(path);
  }

  // SQL string to create the database
  Future _onCreate(Database db, int version) async {
    await db.execute('''
              CREATE TABLE $tableProductos (
                $columnId INTEGER PRIMARY KEY,
                $columnIdProducto INTEGER NOT NULL,
                $columnNombre TEXT NOT NULL,
                $columnPrecio INTEGER NOT NULL,
                $columnFoto TEXT NOT NULL,
                $columnCantidad INTEGER NOT NULL,
                $columnTotal INTEGER NOT NULL
              )
              ''');
  }

  // Database helper methods:

  Future<int> insert(Producto producto) async {
    Database db = await database;
    int id = await db.insert(tableProductos, producto.toMap());
    return id;
  }

  Future<Producto> queryProducto(int id) async {
    Database db = await database;
    List<Map> maps = await db.query(tableProductos,
        columns: [columnId, columnIdProducto, columnNombre, columnPrecio, columnFoto, columnCantidad, columnTotal],
        where: '$columnId = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return Producto.fromMap(maps.first);
    }
    return null;
  }

   Future<int> update(Producto producto) async {
     Database db = await database;
    return await db.update(tableProductos, producto.toMap(),
        where: '$columnId = ?', whereArgs: [producto.id]);
  }

  Future<int> delete(int id) async {
     Database db = await database;
    return await db.delete(tableProductos, where: '$columnId = ?', whereArgs: [id]);
  }


 Future<int> deleteDb() async {
    _deleteDatabase();
    /*
  _deleteDb() async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.deleteDb();
    print('Deleted database');
  }*/
  }

 



  // TODO: queryAllWords()
  // TODO: delete(int id)
  // TODO: update(Word word)
}
