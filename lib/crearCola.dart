import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'dart:convert';
import 'package:http_parser/http_parser.dart';
import 'package:mime/mime.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'dart:math'; 
import 'package:siguelo_app/home.dart';

final _nombresController = TextEditingController();
final _personasController = TextEditingController();

class CrearColaPage extends StatefulWidget {
  CrearColaPage({
    Key key,
    this.idUsuario,
    this.token,
    this.idNegocio,
    this.status
    
  }) : super(key: key);

  var idUsuario;
  var idNegocio;
  var producto;
  String token;
  String status;
  @override
  _CrearColaPage createState() => _CrearColaPage();
}

class _CrearColaPage extends State<CrearColaPage> {


  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  

String status = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: ListView(
          children: <Widget>[
            widgetProductos(),
          ],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }

    Widget widgetTituloMobile() {
    return Container(
      padding: const EdgeInsets.only(bottom: 20.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Crear Cola",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 40),
          ),
        ));
  }


  widgetProductos(){

    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 10.0),
        child: Column(children: <Widget>[
          widgetTituloMobile(),
          Container(
        padding: const EdgeInsets.only(bottom: 20.0),
        child: TextFormField(
            controller: _nombresController,
            decoration: const InputDecoration(
              hintText: 'Nombre',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          )),
       
           Container(
        padding: const EdgeInsets.only(bottom: 20.0),
        child: TextFormField(
            controller: _personasController,
            decoration: const InputDecoration(
              hintText: 'Personas por franja',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          )),
          if(widget.status == "Crear")
          RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {
                      //sendRequest();
                      
                      crearCola(_nombresController.text, _personasController.text);
                    },
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Crear",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  ),
                  if(widget.status == "Editar")
          RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {
                      //sendRequest();
                      
                      editarCola(_nombresController.text, _personasController.text);
                    },
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Editar",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  ),
          
          ]));
  }


  
crearCola(nombre, personas) async{

  Map<String, dynamic> data = {
      'name': nombre,
      'people_per_time': personas.toString(),
      'servicio': widget.idNegocio.toString(),

    };

    var response = await http.post(
        'http://50.116.46.197:8000/api/1.0/queues/',
        body: data,
        headers: {'Authorization': "Token " + widget.token});


    var jsonResponse = json.decode(response.body);
    print(response.statusCode);
    print(jsonResponse);

    if(response.statusCode == 201){

colaCreada();
    }
    else{
      problema();
    }
}


  Future<void> colaCreada() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Cola Creada'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('La cola se creo correctamente'),
              ],
            ),
          ),
          actions: <Widget>[

            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                     Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePageLogin( token: widget.token, idUsuario: widget.idUsuario)), (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> problema() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Hubo un problema'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('La cola no se pudo se crear correctamente, por favor revise los datos'),
              ],
            ),
          ),
          actions: <Widget>[

            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                     Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }



editarCola(nombre, personas) async{


colaEditada();
}



  Future<void> colaEditada() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Producto Editado'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('La cola se edito correctamente'),
              ],
            ),
          ),
          actions: <Widget>[

            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                     Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePageLogin( token: widget.token, idUsuario: widget.idUsuario)), (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }
}