import 'package:flutter/material.dart';


class OrdenesProveedorPage extends StatefulWidget {
  OrdenesProveedorPage({
    Key key,
    this.title,
    this.pedidos,
  }) : super(key: key);

  final String title;
  List pedidos;

  @override
  _OrdenesProveedorPage createState() => _OrdenesProveedorPage();
}

class _OrdenesProveedorPage extends State<OrdenesProveedorPage> {
//List pedidos = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  /*Future obtenerPedidos() async {
    final prefs = await SharedPreferences.getInstance();
    final myStringList = prefs.getStringList('datosInicio') ?? [];

    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/pedidos-cliente/',
        headers: {'Authorization': "Token " + myStringList[0]});
    var data = jsonDecode(response.body);
    if(response.statusCode == 200){
    pedidos = data;
    }
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: ListView(
          children: <Widget>[
            widgetPedidos(),
          ],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }

  widgetPedidos() {
    if (widget.pedidos.isNotEmpty) {
    return Expanded(
        child: ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            scrollDirection: Axis.vertical,
            itemCount: widget.pedidos.length,
            padding: const EdgeInsets.all(8),
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                        color: Color.fromRGBO(54, 185, 204, 1),
                        width: MediaQuery.of(context).size.width,
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: "Número de orden: " +
                                widget.pedidos[index]["id"].toString(),
                            style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontSize: 20),
                          ),
                        )),
                        if(widget.pedidos[index]["fecha_envio"] != null)
                    Container(
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            text: widget.pedidos[index]["fecha_envio"].toString().substring(0, 10) + " a las " +
                            widget.pedidos[index]["fecha_envio"].toString().substring(11, 16),
                            style: TextStyle(
                                color: Color.fromRGBO(0, 0, 0, 1),
                                fontSize: 20),
                          ),
                        )),
                    Row(children: <Widget>[
                      Expanded(
                          child: ListView.builder(
                              physics: const NeverScrollableScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              itemCount: widget.pedidos[index]["productos"].length,
                              padding: const EdgeInsets.all(8),
                              shrinkWrap: true,
                              itemBuilder: (cont, ind) {
                                return Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: RichText(
                                      textAlign: TextAlign.left,
                                      text: TextSpan(
                                        text: widget.pedidos[index]["productos"][ind]
                                                ["nombre"]
                                            .toString(),
                                        style: TextStyle(
                                            color: Color.fromRGBO(0, 0, 0, 1),
                                            fontSize: 20),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text: ' x ',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                          TextSpan(
                                              text: widget.pedidos[index]["productos"][ind]
                                                      ["cantidad"]
                                                  .toString()),
                                        ],
                                      ),
                                    ));
                              })),
                    ]),
                    if(widget.pedidos[index]["verificado_cliente"] == false)
                    Container(
                      color: Color.fromRGBO(54, 185, 204, 1),
                      width: MediaQuery.of(context).size.width,
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(                              
                              text: "No confirmada por cliente",
                              style: TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 1),
                                  fontSize: 20),
                            ),
                          )),
                    if(widget.pedidos[index]["verificado_cliente"] == true && widget.pedidos[index]["orden_enviada"] == true)
                    Container(
                      color: Color.fromRGBO(54, 185, 204, 1),
                      width: MediaQuery.of(context).size.width,
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(                              
                              text: "orden enviada",
                              style: TextStyle(
                                  color: Color.fromRGBO(0, 0, 0, 1),
                                  fontSize: 20),
                            ),
                          )),
                    if(widget.pedidos[index]["verificado_cliente"] == true && widget.pedidos[index]["orden_enviada"] == false)
                    Container(
                      color: Color.fromRGBO(231, 74, 59, 1),
                      width: MediaQuery.of(context).size.width,
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(                              
                              text: "orden no enviada",
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  fontSize: 20),
                            ),
                          )),
                          RaisedButton(
                      color: Color.fromRGBO(255, 0, 0, 1),
                      onPressed: () {
                        
                      },
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: "Eliminar",
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 18),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }));
    } else {
      return Expanded(
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "No tienes pedidos!",
            style: TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
          ),
        ),
      );
    }
}
}