import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'home.dart';
import 'package:intl/intl.dart';
import 'package:geolocator/geolocator.dart';

final _nombresController = TextEditingController();
final _apellidosController = TextEditingController();
final _telefonoController = TextEditingController();
final _numDocController = TextEditingController();

String dropdownValue = 'Tipo de Documento';

class FormularioPage extends StatefulWidget {
  FormularioPage(
      {Key key,
      this.username,
      this.password,
      this.token,
      this.idUsuario,
      this.status})
      : super(key: key);

  String username;
  String password;
  String token;
  int idUsuario;
  String status;

  @override
  _FormularioPage createState() => _FormularioPage();
}

class _FormularioPage extends State<FormularioPage> {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  DateTime fechaNac;
  String fechaView;
  String abrevDoc;
  Position _currentPosition;
  String _currentAddress = "";
  String departamento;
  String ciudad;
  String direccion;
  int idDepartamento;
  int idCiudad;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: ListView(
          children: <Widget>[
            widgetTituloMobile(),
            widgetFormulario(),
          ],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }

  Widget widgetTituloMobile() {
    if (widget.status == "new") {
    return Container(
        margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Crear Perfil",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 50),
          ),
        ));
    }else if (widget.status == "old") {
      return Container(
        margin: const EdgeInsets.only(top: 20.0, bottom: 20.0),
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Editar Perfil",
            style:
                TextStyle(color: Color.fromRGBO(193, 29, 27, 1), fontSize: 50),
          ),
        ));
    }
  }

  Widget widgetFormulario() {
    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 50.0),
        child: Column(children: <Widget>[
          TextFormField(
            controller: _nombresController,
            decoration: const InputDecoration(
              hintText: 'Nombres',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          ),
          TextFormField(
            controller: _apellidosController,
            decoration: const InputDecoration(
              hintText: 'Apellidos',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          ),
          TextFormField(
            controller: _telefonoController,
            decoration: const InputDecoration(
              hintText: 'Número Telefónico',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          ),
          Container(
              height: 60.0,
              padding: const EdgeInsets.only(left: 10.0, top: 5.0),
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(
                    width: 1,
                    color: Color.fromRGBO(120, 120, 120, 1),
                  ),
                  left: BorderSide(
                    width: 1,
                    color: Color.fromRGBO(120, 120, 120, 1),
                  ),
                  right: BorderSide(
                    width: 1,
                    color: Color.fromRGBO(120, 120, 120, 1),
                  ),
                  bottom: BorderSide(
                    width: 1,
                    color: Color.fromRGBO(120, 120, 120, 1),
                  ),
                ),
                borderRadius: BorderRadius.all(
                    Radius.circular(5.0) //         <--- border radius here
                    ),
              ),
              child: DropdownButton<String>(
                isExpanded: true,
                value: dropdownValue,
                icon: Icon(Icons.arrow_downward),
                iconSize: 30,
                style: TextStyle(
                    color: Color.fromRGBO(120, 120, 120, 1), fontSize: 15),
                onChanged: (String newValue) {
                  setState(() {
                    dropdownValue = newValue;
                  });
                },
                items: <String>[
                  'Tipo de Documento',
                  'Cédula de Ciudadanía',
                  'Cédula de Extranjería',
                  'Pasaporte'
                ].map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              )),
          TextFormField(
            controller: _numDocController,
            decoration: const InputDecoration(
              hintText: 'Número de Documento',
              border: const OutlineInputBorder(),
              filled: true,
              fillColor: Colors.white,
            ),
          ),
          if (fechaNac == null)
            InkWell(
                onTap: () {
                  showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(1920),
                          lastDate: DateTime.now())
                      .then((date) {
                    setState(() {
                      fechaNac = date;
                      fechaView = DateFormat('yyyy-MM-dd').format(fechaNac);
                    });
                  });
                },
                child: Container(
                  padding: const EdgeInsets.only(left: 10.0, top: 18.0),
                  width: MediaQuery.of(context).size.width,
                  height: 60.0,
                  decoration: const BoxDecoration(
                    border: Border(
                      top: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                      left: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                      right: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                      bottom:
                          BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    ),
                    borderRadius: BorderRadius.all(
                        Radius.circular(5.0) //         <--- border radius here
                        ),
                  ),
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: "Fecha de nacimiento",
                      style: TextStyle(
                          color: Color.fromRGBO(120, 120, 120, 1),
                          fontSize: 18),
                    ),
                  ),
                )),
          if (fechaNac != null)
            InkWell(
                onTap: () {
                  showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(1920),
                          lastDate: DateTime.now())
                      .then((date) {
                    setState(() {
                      fechaNac = date;
                      fechaView = DateFormat('yyyy-MM-dd').format(fechaNac);
                    });
                  });
                },
                child: Container(
                  padding: const EdgeInsets.only(left: 10.0, top: 18.0),
                  width: MediaQuery.of(context).size.width,
                  height: 60.0,
                  decoration: const BoxDecoration(
                    border: Border(
                      top: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                      left: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                      right: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                      bottom:
                          BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                    ),
                    borderRadius: BorderRadius.all(
                        Radius.circular(5.0) //         <--- border radius here
                        ),
                  ),
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: fechaView,
                      style: TextStyle(
                          color: Color.fromRGBO(120, 120, 120, 1),
                          fontSize: 18),
                    ),
                  ),
                )),
          Container(
              width: MediaQuery.of(context).size.width / 1.3,
              child: RaisedButton(
                onPressed: () {
                  _getCurrentLocation();
                },
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: "Obtener ubicación actual",
                    style: TextStyle(
                        color: Color.fromRGBO(193, 29, 27, 1), fontSize: 18),
                  ),
                ),
              )),
          if (_currentPosition == null)
            Container(
              padding: const EdgeInsets.only(left: 10.0, top: 20.0),
              width: MediaQuery.of(context).size.width,
              height: 60.0,
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                  left: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                  right: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                  bottom: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                ),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
              child: RichText(
                textAlign: TextAlign.left,
                text: TextSpan(
                  text: "Ubicación",
                  style: TextStyle(
                      color: Color.fromRGBO(120, 120, 120, 1), fontSize: 18),
                ),
              ),
            ),
          if (_currentPosition != null)
            Container(
              padding: const EdgeInsets.only(left: 10.0, top: 8.0),
              width: MediaQuery.of(context).size.width,
              height: 60.0,
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                  left: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                  right: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                  bottom: BorderSide(width: 0.8, color: Color(0xFFFF000000)),
                ),
                borderRadius: BorderRadius.all(
                    Radius.circular(5.0) //         <--- border radius here
                    ),
              ),
              child: RichText(
                textAlign: TextAlign.left,
                text: TextSpan(
                  text: _currentAddress,
                  style: TextStyle(
                      color: Color.fromRGBO(120, 120, 120, 1), fontSize: 18),
                ),
              ),
            ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: buttonAddFormulario(),
          )
        ]));
  }

// FUNCIONES //

  buttonAddFormulario() {
    if (dropdownValue == 'Cédula de Ciudadanía') {
      abrevDoc = "CC";
    } else if (dropdownValue == 'Cédula de Extranjería') {
      abrevDoc = "CE";
    } else if (dropdownValue == 'Pasaporte') {
      abrevDoc = "PTE";
    }

    if (widget.status == "new") {
      return RaisedButton(
        color: Color.fromRGBO(193, 29, 27, 1),
        onPressed: () {
          if (_nombresController.text == "" ||
              _apellidosController.text == "" ||
              _telefonoController.text == "" ||
              dropdownValue == 'Tipo de Documento' ||
              _numDocController.text == "" ||
              fechaView == null ||
              _currentAddress == "") {
            _campoVacio();
          } else {
            addFormulario(
                _nombresController.text,
                _apellidosController.text,
                _telefonoController.text,
                _numDocController.text);
          }
        },
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Crear",
            style: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 1), fontSize: 18),
          ),
        ),
      );
    } else if (widget.status == "old") {
      return RaisedButton(
        color: Color.fromRGBO(193, 29, 27, 1),
        onPressed: () {
          editFormulario(
              _nombresController.text,
                _apellidosController.text,
                _telefonoController.text,
                _numDocController.text);
        },
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "Editar perfil",
            style: TextStyle(
                color: Color.fromRGBO(255, 255, 255, 1), fontSize: 18),
          ),
        ),
      );
    }
  }

  addFormulario(
      nombre, apellidos, telefono, numDoc) async {
    Map<String, dynamic> data = {
      'nombres': nombre,
      'apellidos': apellidos,
      'telefono': telefono,
      'tipo_dni': abrevDoc,
      'dni': numDoc,
      'fecha_nacimiento': fechaView,
      'departamento': idDepartamento.toString(),
      'ciudad': idCiudad.toString(),
      'direccion': direccion,
      'lon': _currentPosition.longitude.toString(),
      'lat': _currentPosition.latitude.toString(),
      'ingresos': 'false'
    };
 

    var response = await http.post(
        'http://50.116.46.197:8000/api/1.0/me/perfil/create/',
        body: data,
        headers: {'Authorization': "Token " + widget.token});


    var jsonResponse = json.decode(response.body);

    if (response.statusCode == 201) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (BuildContext context) => HomePageLogin(
                  token: widget.token, idUsuario: widget.idUsuario)),
          (Route<dynamic> route) => false);
    }
  }







  editFormulario(
       nombre, apellidos, telefono, numDoc) async {
        var newLat;
        var newLon;
  
      final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/me/perfil/' + widget.idUsuario.toString() + '/',
        headers: {'Authorization': "Token " + widget.token,
        'Content-type': 'application/json, charset: "utf-8'});

        
    var datas = utf8.decode(response.bodyBytes);
    var data = jsonDecode(datas);
   
    if(_nombresController.text == ""){
      nombre = data["nombres"];
    }

    if(_apellidosController.text == ""){
      apellidos = data["apellidos"];
    }

    if(_telefonoController.text == ""){
      telefono = data["telefono"];
    }

    if(dropdownValue == 'Tipo de Documento'){
      abrevDoc = data["tipo_dni"];
    }

    if(_numDocController.text == ""){
      numDoc = data["dni"];
    }

    if(fechaView == null){
      fechaView = data["fecha_nacimiento"];
    }

    if(_currentAddress == ""){
      idDepartamento = data["departamento"];      
      idCiudad = data["ciudad"];
      direccion =  data["direccion"];
      newLat = data["lat"].toString();
      newLon = data["lon"].toString();
    }else{
      newLat =  _currentPosition.latitude.toString();
      newLon = _currentPosition.longitude.toString();
    }


    

    Map dataEdit = {
      'nombres': nombre,
      'apellidos': apellidos,
      'telefono': telefono,
      'tipo_dni': abrevDoc,
      'dni': numDoc,
      'fecha_nacimiento': fechaView,
      'departamento': idDepartamento.toString(),
      'ciudad': idCiudad.toString(),
      'direccion': direccion,
      'lon': newLon,
      'lat': newLat
    };

    var response1 = await http.put(
        'http://50.116.46.197:8000/api/1.0/me/perfil/update/' + widget.idUsuario.toString() + '/',  body: dataEdit,

        headers: {'Authorization': "Token " + widget.token});
        var data1 = jsonDecode(response1.body);

        if(response.statusCode == 200){
        _editado(data1);
        
        }
  }

  Future<void> _editado(data) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Se editó la información con éxito'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text('Se cambio la información suministrada, muchas gracias'),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}


  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
      _getDepartamentoId();
      _getMunicipioId();
      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);
      /*List<Placemark> p = await geolocator.placemarkFromCoordinates(
         4.444957, -69.798932);*/

      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.name}, ${place.thoroughfare}, ${place.subThoroughfare},${place.locality}, ${place.administrativeArea.toUpperCase()}";
        departamento = place.administrativeArea;
        ciudad = place.locality;
        direccion = place.thoroughfare + place.subThoroughfare;
      });
    } catch (e) {
      print(e);
    }
  }

  _getDepartamentoId() async {
    var response =
        await http.get('http://50.116.46.197:8000/api/1.0/departamentos/');
    var jsonResponse = json.decode(response.body);

    for (var i = 0; i < 33; i++) {
      if (jsonResponse[i]["nombre_departamento"] ==
          departamento.toUpperCase()) {
        idDepartamento = jsonResponse[i]["codigo_departamento"];
      } else if (departamento.toUpperCase() == "SAN ANDRÉS Y PROVIDENCIA") {
        idDepartamento = jsonResponse[27]["codigo_departamento"];
      } else if (departamento.toUpperCase() == "BOGOTÁ") {
        idDepartamento = jsonResponse[2]["codigo_departamento"];
      }
    }
  }

  _getMunicipioId() async {
    var response =
        await http.get('http://50.116.46.197:8000/api/1.0/municipios/');
    var jsonResponse = json.decode(response.body);
    for (var i = 0; i < 1121; i++) {
      if (jsonResponse[i]["nombre_municipio"] == ciudad.toUpperCase()) {
        idCiudad = jsonResponse[i]["codigo_dpto_mpio"];
      }
    }
  }

  Future<void> _campoVacio() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Hay un campo vacio'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Por favor llene todos los campos'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
