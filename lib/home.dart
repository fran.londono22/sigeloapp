import 'dart:async';
import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:geolocator/geolocator.dart';
import 'package:siguelo_app/carritoDeCompras.dart';
import 'package:siguelo_app/login.dart';
import 'package:siguelo_app/panelControl.dart';
import 'package:siguelo_app/tiendaServicio.dart';
import 'package:siguelo_app/tiendasServiciosPropias.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_open_whatsapp/flutter_open_whatsapp.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'negocio.dart';
import 'negociosPropios.dart';
import 'package:dio/dio.dart';

class HomePageLogin extends StatefulWidget {
  HomePageLogin({Key key, this.token, this.idUsuario}) : super(key: key);

  String token;
  int idUsuario;
    final List<String> list = List.generate(10, (index) => "Texto $index");


  @override
  _HomePage createState() => _HomePage();
}


class _HomePage extends State<HomePageLogin> {
  //variables
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Position _currentPosition;
  Timer timer;
  double lat;
  double lon;
  bool press = false;
  String tipo;
  final _search = TextEditingController();
  String dropdownValue = 'Productos';
  var places;
  int idNegocio;
  int idServicio;
  var estadoUsuario;


  @override
  void initState() {
    _getCurrentLocation();
    setState(() {
      tipoUsuario();
    });

    //timer = Timer.periodic(Duration(seconds: 3), (Timer t) => tipoUsuario());
    super.initState();
    tipoUsuario();
    markerPlaces();
  }

  @override
  void dispose() {
    //timer?.cancel();
    super.dispose();
  }

  //widgets

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(255, 255, 255, 1),
            title: appBarMobile(),
            iconTheme:
                new IconThemeData(color: Color.fromRGBO(179, 179, 179, 1)),
          ),
          body: SafeArea(
              child: Container(
            color: Color.fromRGBO(255, 255, 255, 1),
            child: Stack(
              //physics: const NeverScrollableScrollPhysics(),
              children: <Widget>[widgetMapa(), //widgetsearch(), 
              widgetCard()],
            ),
          )),
          drawer: Drawer(
            child: widgetMenuLateral(),
          ),
        ));
  }

  appBarMobile() {
    return Container(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[widgetLogo(), widgetCarrito()]));
  }

  widgetCarrito() {
    return IconButton(
      icon: Icon(Icons.shopping_cart),
      color: Color.fromRGBO(193, 29, 27, 1),
      iconSize: 30.0,
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CarritoPage(token: widget.token)),
        );
      },
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  widgetMapa() {
    return Container(
        height: 1000,
        child: FlutterMap(
            options: new MapOptions(
                center: LatLng(
                    _currentPosition.latitude, _currentPosition.longitude),
                minZoom: 2.0),
            layers: [
              new TileLayerOptions(
                  urlTemplate:
                      "https://api.mapbox.com/styles/v1/pacho0522/ckdex357s5hnv1inz5r1kbbog/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoicGFjaG8wNTIyIiwiYSI6ImNrZGFybWt6eTA3NzIycHQ4YTlnYjRuanUifQ.qXv2hwJ7NKNrKZDU5kR4dQ",
                  additionalOptions: {
                    'accessToken':
                        'pk.eyJ1IjoicGFjaG8wNTIyIiwiYSI6ImNrZGV3Yzc3cTBiajkycW5hczhzaG00cWUifQ.TrdKX7gvxKd3ghC-_uw8aA',
                    'id': 'mapbox.mapbox-streets-v7'
                  }),
              //if(places.isNotEmpty)
              MarkerLayerOptions(
                markers: _buildMarkersOnMap(),
              )
            ]));
  }

  widgetsearch() {
    return 
       Container(
        alignment: Alignment.topCenter,
        padding: new EdgeInsets.only(
            top: MediaQuery.of(context).size.height * 0,
            right: 20.0,
            left: 20.0),
        child: new Container(
          height: 500.0,
          width: MediaQuery.of(context).size.width,
          child: Column(children: <
              Widget>[            
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <
              Widget>[
            new Flexible(
                child: new TextField(
              controller: _search,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(0)),
                    borderSide: BorderSide(
                      width: 1,
                    )),
                hintText: 'Buscar en el mapa',
                labelText: 'Buscar',
                prefixIcon: const Icon(
                  Icons.search,
                  color: Color.fromRGBO(193, 29, 27, 1),
                ),
                filled: true,
                fillColor: Color.fromRGBO(255, 255, 255, 1),
              ),
            )),
            Flexible(
                child: Theme(
                    data: Theme.of(context).copyWith(
                      canvasColor: Color.fromRGBO(193, 29, 27, 1),
                    ),
                    child: Container(
                        height: 62.0,
                        padding: const EdgeInsets.only(left: 10.0, top: 5.0),
                        decoration: const BoxDecoration(
                          color: Color.fromRGBO(193, 29, 27, 1),
                          border: Border(
                            top: BorderSide(
                              width: 1,
                              color: Color.fromRGBO(193, 29, 27, 1),
                            ),
                            left: BorderSide(
                              width: 1,
                              color: Color.fromRGBO(193, 29, 27, 1),
                            ),
                            right: BorderSide(
                              width: 1,
                              color: Color.fromRGBO(193, 29, 27, 1),
                            ),
                            bottom: BorderSide(
                              width: 1,
                              color: Color.fromRGBO(193, 29, 27, 1),
                            ),
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(
                                  0) //         <--- border radius here
                              ),
                        ),
                        child: DropdownButton<String>(
                          value: dropdownValue,
                          underline: SizedBox(),
                          icon: Icon(Icons.arrow_downward,
                              color: Color.fromRGBO(255, 255, 255, 1)),
                          iconSize: 15,
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 15),
                          onChanged: (String newValue) {
                            setState(() {
                              dropdownValue = newValue;
                            });
                          },
                          items: <String>[
                            'Productos',
                            'Servicios',
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        )))),
          ]),
          /*Container(
            height: 200.0,
            width: MediaQuery.of(context).size.width,
            child: _buildList(),)*/
          ]),
        ));
  }


  widgetCard() {
    if (press == true) {
      return Container(
          alignment: Alignment.topCenter,
          padding: new EdgeInsets.only(
              top: MediaQuery.of(context).size.height * .70,
              right: 20.0,
              left: 20.0),
          child: new Container(
            height: 120.0,
            width: MediaQuery.of(context).size.width,
            child: new Card(
              color: Colors.white,
              elevation: 4.0,
              child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                if (tipo == "negocio")
                  Text(places["negocios"][idNegocio]["nombre"]),
                if (tipo == "servicio")
                  Text(places["servicios"][idServicio]["nombre"]),
                if (tipo == "negocio" &&
                    places["negocios"][idNegocio]["domicilio"] == true)
                  Text("domicilio"),
                if (tipo == "negocio" &&
                    places["negocios"][idNegocio]["domicilio"] == true)
                  Text(places["negocios"][idNegocio]["costo"]),
                Container(
                    padding: new EdgeInsets.only(right: 5.0, left: 5.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          RaisedButton(
                            color: Color.fromRGBO(193, 29, 27, 1),
                            onPressed: () {
                              if (tipo == "negocio") {
                                abrirNegocio(
                                    places["negocios"][idNegocio]["id"]);
                              }
                              if (tipo == "servicio") {
                                abrirServicio(
                                    places["servicios"][idServicio]["id"]);
                              }
                            },
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text: tipo == "negocio"
                                    ? "Visitar tienda"
                                    : "Ver servicio",
                                //text: "Visitar tienda",
                                style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontSize: 18),
                              ),
                            ),
                          ),
                          RaisedButton(
                            color: Color.fromRGBO(28, 200, 138, 1),
                            onPressed: () {
                              FlutterOpenWhatsapp.sendSingleMessage(
                                  places["negocios"][idNegocio]["telefono"],
                                  "Hola quisiera tener mas información del negoció");
                            },
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                children: [
                                  WidgetSpan(
                                      child: Container(
                                          width: 25,
                                          height: 25,
                                          child: Image.network(
                                            'https://i.pinimg.com/originals/79/dc/31/79dc31280371b8ffbe56ec656418e122.png',
                                          ))),
                                  TextSpan(
                                    text: " Enviar mensaje",
                                    style: TextStyle(
                                        color: Color.fromRGBO(255, 255, 255, 1),
                                        fontSize: 18),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ])),
              ]),
            ),
          ));
    } else {
      return Column();
    }
  }

  Widget widgetMenuLateral() {
    return ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          child: Container(
              decoration: new BoxDecoration(
                  image: new DecorationImage(
            image: new AssetImage("assets/images/logo.png"),
          ))),
          decoration: BoxDecoration(
            color: Color.fromRGBO(179, 179, 179, 1),
          ),
        ),
        ListTile(
          title: Text('Inicio'),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: Text('Mi panel de control'),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PanelControlPage(
                      token: widget.token,
                      idUsuario: widget.idUsuario,
                      estado: estadoUsuario)),
            );
          },
        ),
        if (estadoUsuario == true)
          ListTile(
            title: Text('Tus negocios aquí!'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => NegociosPage(
                        idUsuario: widget.idUsuario, token: widget.token)),
              );
            },
          ),
        if (estadoUsuario == true)
          ListTile(
            title: Text('Tus servicios aquí!'),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => TiendaServiciosPage(
                        idUsuario: widget.idUsuario, token: widget.token)),
              );
            },
          ),
        ListTile(
          title: Text('Encuestas donaciones'),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePageLogin()),
            );
          },
        ),
        ListTile(
          title: Text('Salir'),
          onTap: () {
            _removePreferences();
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (BuildContext context) => LoginPage()),
                (Route<dynamic> route) => false);
          },
        ),
      ],
    );
  }

  //Funciones de consulta al API

  Future markerPlaces() async {
    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/list-map/',
        headers: {'Authorization': "Token " + widget.token});
    var data = jsonDecode(response.body);
    places = data;
  }

  Future tipoUsuario() async {
    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/me/perfil/' +
            widget.idUsuario.toString() +
            '/',
        headers: {'Authorization': "Token " + widget.token});
    var data = jsonDecode(response.body);
    setState(() {
      estadoUsuario = data["ingresos"];
    });
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool('ingresos', estadoUsuario);
  }

  Future<bool> _onBackPressed() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Salir de la aplicación'),
            content: new Text('¿Quiere salir de la aplicación?'),
            actions: <Widget>[
              new FlatButton(
                child: new Text("Si"),
                onPressed: () {
                  Navigator.of(context).pop(true);
                },
              ),
              new FlatButton(
                child: new Text("No"),
                onPressed: () {
                  Navigator.of(context).pop(false);
                },
              ),
              SizedBox(height: 16),
            ],
          ),
        ) ??
        false;
  }

  abrirNegocio(idNegocio) async {
    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/negocio/' +
            idNegocio.toString() +
            '/',
        headers: {
          'Authorization': 'Token ' + widget.token,
          'Content-type': 'application/json'
        });
    var data = utf8.decode(response.bodyBytes);
    var infoNegocio = jsonDecode(data);

    final response1 = await http.get(
        'http://50.116.46.197:8000/api/1.0/negocio/detail/' +
            idNegocio.toString() +
            '/',
        headers: {
          'Authorization': 'Token ' + widget.token,
          'Content-type': 'application/json'
        });
    var data1 = utf8.decode(response1.bodyBytes);
    var productos = jsonDecode(data1);

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => NegocioPage(
              token: widget.token,
              idNegocio: idNegocio,
              infoNegocio: infoNegocio,
              productos: productos)),
    );
  }

  abrirServicio(idServicio) async {
    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/tienda-servicios/' +
            idServicio.toString() +
            '/',
        headers: {
          'Authorization': 'Token ' + widget.token,
          'Content-type': 'application/json'
        });
    var data = utf8.decode(response.bodyBytes);
    var infoServicio = jsonDecode(data);

    final response1 = await http.get(
        'http://50.116.46.197:8000/api/1.0/servicios/detail/' +
            idServicio.toString() +
            '/',
        headers: {
          'Authorization': 'Token ' + widget.token,
          'Content-type': 'application/json'
        });
    var data1 = utf8.decode(response1.bodyBytes);
    var servicios = jsonDecode(data1);

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => TiendaPage(
              token: widget.token,
              idServicio: idServicio,
              infoServicio: infoServicio,
              servicios: servicios)),
    );
  }

  List<Marker> _buildMarkersOnMap() {
    List<Marker> markers = List<Marker>();

    for (var i = 0; i < places["negocios"].length; i++) {
      var marker = new Marker(
          width: 45.0,
          height: 45.0,
          point: new LatLng(
              places["negocios"][i]["lon"], places["negocios"][i]["lat"]),
          builder: (context) => new Container(
                child: IconButton(
                  icon: iconos(i),
                  color: Colors.red,
                  iconSize: 30.0,
                  onPressed: () {
                    if (press == false) {
                      setState(() {
                        press = true;
                        tipo = "negocio";
                        idNegocio = i;
                      });
                    } else if (press == true && idNegocio == i) {
                      setState(() {
                        press = false;
                      });
                    } else {
                      setState(() {
                        idNegocio = i;
                      });
                    }
                  },
                ),
              ));
      markers.add(marker);
    }

    for (var j = 0; j < places["servicios"].length; j++) {
      var marker = new Marker(
          width: 45.0,
          height: 45.0,
          point: new LatLng(
              places["servicios"][j]["lon"], places["servicios"][j]["lat"]),
          builder: (context) => new Container(
                child: IconButton(
                  icon: Icon(Icons.location_on),
                  color: Colors.blue,
                  iconSize: 30.0,
                  onPressed: () {
                    if (press == false) {
                      setState(() {
                        press = true;
                        tipo = "servicio";
                        idServicio = j;
                      });
                    } else if (press == true && idServicio == j) {
                      setState(() {
                        press = false;
                      });
                    } else {
                      setState(() {
                        idServicio = j;
                      });
                    }
                  },
                ),
              ));
      markers.add(marker);
    }

    return markers;
  }

  iconos(i) {
    /*
      1 = Tienda de barrio
      2 = Comidas rápidas
      3 = Panaderías
      4 = Minimercados
      5 = Supermercados
      6 = Droguerías
      7 = Licorerías-Cigarrerías
      8 = Cafeterías
      9 = Restaurantes
      10 = otros */

    if (places["negocios"][i]["categoria"] == 1) {
      return Icon(Icons.store);
    } else if (places["negocios"][i]["categoria"] == 2) {
      return Icon(Icons.fastfood);
    } else if (places["negocios"][i]["categoria"] == 3) {
      return Icon(Icons.cake);
    } else if (places["negocios"][i]["categoria"] == 4) {
      return Icon(Icons.shopping_basket);
    } else if (places["negocios"][i]["categoria"] == 5) {
      return Icon(Icons.local_mall);
    } else if (places["negocios"][i]["categoria"] == 6) {
      return Icon(Icons.healing);
    } else if (places["negocios"][i]["categoria"] == 7) {
      return Icon(Icons.local_bar);
    } else if (places["negocios"][i]["categoria"] == 8) {
      return Icon(Icons.local_cafe);
    } else if (places["negocios"][i]["categoria"] == 9) {
      return Icon(Icons.restaurant);
    } else if (places["negocios"][i]["categoria"] == 10) {
      return Icon(Icons.location_on);
    } else {
      return Icon(Icons.location_on);
    }
  }

  _getCurrentLocation() async {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        lat = _currentPosition.latitude;
        lon = _currentPosition.longitude;
      });
    }).catchError((e) {
      print(e);
    });
  }

  _removePreferences() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('datosInicio');
  }
}
