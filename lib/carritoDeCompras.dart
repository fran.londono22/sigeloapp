import 'package:flutter/material.dart';
import './db/database_helpers.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'crearNegocio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

class CarritoPage extends StatefulWidget {
  CarritoPage({Key key, this.username, this.password, this.token})
      : super(key: key);

  String username;
  String password;
  String token;

  @override
  _CarritoPage createState() => _CarritoPage();
}

class ProductData {
  int id;
  int idProducto;
  String nombre;
  int precio;
  String foto;
  int cantidad;
  int total;

  ProductData(id, idProducto, nombre, precio, foto, cantidad, total) {
    this.id = id;
    this.idProducto = idProducto;
    this.nombre = nombre;
    this.precio = precio;
    this.foto = foto;
    this.cantidad = cantidad;
    this.total = total;
  }
}

class _CarritoPage extends State<CarritoPage> {
  int productosTotales = 0;
  int totalPago = 0;
  bool eliminado;

  List<ProductData> data = [];

  _read() async {
    final prefs = await SharedPreferences.getInstance();
    final ultimoProducto = prefs.getInt('ultimoProducto') ?? 0;
    DatabaseHelper helper = DatabaseHelper.instance;

    for (var i = 1; i <= ultimoProducto; i++) {
      int rowId = i;
      Producto producto = await helper.queryProducto(rowId);

      if (data.length != i && producto != null) {
        data.add(ProductData(rowId, producto.idProducto, producto.nombre, producto.precio,
            producto.foto, producto.cantidad, producto.total));
      }
      if (producto != null) {
        productosTotales = productosTotales + producto.cantidad;
        totalPago = totalPago + (producto.cantidad * producto.precio);
      }

    }
  }

  @override
  void initState() {
    _read().then((result) {
      setState(() {});
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(220, 220, 220, 1),
        child: Column(
          children: <Widget>[
            wigdetResumenCompra(),
            widgetTotal(),
            wigdetBtnComprar(),
          ],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }

  wigdetResumenCompra() {
    return Expanded(
        child: ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: data.length,
      padding: const EdgeInsets.all(8),
      shrinkWrap: true,
      itemBuilder: (context, index) {
        _update(
            data[index].id,
            data[index].idProducto,
            data[index].nombre,
            data[index].precio,
            data[index].foto,
            data[index].cantidad,
            data[index].cantidad * data[index].precio);
        return Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(right: 30.0),
                    width: 100.0,
                    height: 100.0,
                    child: Image.network(
                      "http://" + data[index].foto,
                    )),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          text: data[index].id.toString(),
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          text: data[index].nombre,
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                          text: "Precio: \$" + data[index].precio.toString(),
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                        ),
                      ),
                    ]),
              ]),
              ButtonBar(
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.add_circle_outline),
                    color: Color.fromRGBO(193, 29, 27, 1),
                    iconSize: 30.0,
                    onPressed: () {
                      setState(() {
                        productosTotales = productosTotales + 1;
                        data[index].cantidad = data[index].cantidad + 1;
                        data[index].total =
                            data[index].total + data[index].precio;
                        totalPago = totalPago + data[index].precio;
                      });
                    },
                  ),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      text: data[index].cantidad.toString(),
                      style: TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.remove_circle_outline),
                    color: Color.fromRGBO(193, 29, 27, 1),
                    iconSize: 30.0,
                    onPressed: () {
                      if (data[index].cantidad > 1) {
                        setState(() {
                          data[index].cantidad = data[index].cantidad - 1;
                          productosTotales = productosTotales - 1;
                          data[index].total =
                              data[index].total - data[index].precio;
                          totalPago = totalPago - data[index].precio;
                        });
                      } else {
                        int precio = data[index].precio;
                        setState(() {
                          _deleteProducto(data[index].id).then((result) {
                            if (eliminado == true) {
                              productosTotales = productosTotales - 1;
                              totalPago = totalPago - precio;
                            }
                          });
                        });
                      }
                    },
                  ),
                ],
              ),
              Container(
                color: Color.fromRGBO(193, 29, 27, 1),
                width: MediaQuery.of(context).size.width,
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    text: "Total: \$" + data[index].total.toString(),
                    style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1), fontSize: 20),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    ));
  }

  widgetTotal() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
        text: "Productos " +
            productosTotales.toString() +
            " Total pago: \$" +
            totalPago.toString(),
        style: TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
      ),
    );
  }

  Widget wigdetBtnComprar() {
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0),
        child: RaisedButton(
          color: Color.fromRGBO(193, 29, 27, 1),
          onPressed: () {
            setState(() {              
            comprar();
            });
          },
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: "Comprar",
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1), fontSize: 18),
            ),
          ),
        ));
  }

  _update(id, idProducto, nombre, precio, foto, cantidad, total) async {
    Producto producto = Producto();
    producto.id = id;
    producto.idProducto = idProducto;
    producto.nombre = nombre;
    producto.precio = precio;
    producto.foto = foto;
    producto.cantidad = cantidad;
    producto.total = total;
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.update(producto);
  }

  Future<void> _deleteProducto(id) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Eliminar producto'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('¿Desea eliminar el producto?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Si'),
              onPressed: () {
                setState(() {
                  _delete(id);
                  data.removeWhere((item) => item.id == id);
                  Navigator.of(context).pop();
                  eliminado = true;
                });
              },
            ),
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _delete(id) async {
    DatabaseHelper helper = DatabaseHelper.instance;
    await helper.delete(id);
  }


  comprar() async{

        final prefs = await SharedPreferences.getInstance();
        final myStringList = prefs.getStringList('datosInicio') ?? [];
        if(myStringList.isNotEmpty){
        }


    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/me/perfil/' + myStringList[3] + '/',
        headers: {'Authorization': "Token " + widget.token});
    var res = jsonDecode(response.body);

    


    Map compra = {
    'idCliente': int.parse(myStringList[3]),
    'direccion': res["direccion"].toString(),
    'productos': [ 
      for(var i = 0; i < data.length; i++){
            'idProducto': data[i].idProducto,
            'nombre': data[i].nombre.toString(),
            'precio': data[i].precio.toString(),
            'cantidad': data[i].cantidad
      }
    ],
    'total': totalPago.toString()
  };
  print(jsonEncode(compra));

  final response1 = await http.post(
        'http://50.116.46.197:8000/api/1.0/add-cart/',
         body: jsonEncode(compra),
        headers: {'Authorization': "Token " + widget.token});
    //var res1 = jsonDecode(response1.body);
  
  if(response1.statusCode == 200){
    setState(() {      
    _confirmarPedido();
    });
  }
  


  }


   Future<void> _confirmarPedido() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Pedido Enviado'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Por favor confirme su pedido a traves del correo electronico que le enviamos'),
              ],
            ),
          ),
          actions: <Widget>[           
            FlatButton(
              child: Text('OK'),
              onPressed: () { 
                setState(() {
                  
                _deleteAll();
                Navigator.of(context).pop();
                });              
              },
            ),
          ],
        );
      },
    );
  }

  _deleteAll() async {
     final prefs = await SharedPreferences.getInstance();
    final ultimoProducto = prefs.getInt('ultimoProducto') ?? 0;
    setState(() {     
      for (var i = 0; i <= ultimoProducto; i++){
    DatabaseHelper helper = DatabaseHelper.instance;
    helper.delete(i);
     data.removeWhere((item) => item.id == i);
    }
     productosTotales = 0;
      
            totalPago = 0;
      
    });

    
    

}

}