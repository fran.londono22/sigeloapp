import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:siguelo_app/comentarios.dart';
import 'package:siguelo_app/formulario.dart';
import 'package:siguelo_app/tiendasServiciosPropias.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:siguelo_app/verOrdenesProveedor.dart';
import 'package:siguelo_app/verPedidosCliente.dart';
import 'home.dart';
import 'negociosPropios.dart';

class PanelControlPage extends StatefulWidget {
  PanelControlPage(
      {Key key,
      this.title,
      this.username,
      this.password,
      this.token,
      this.idUsuario,
      this.estado})
      : super(key: key);

  String username;
  String password;
  String token;
  int idUsuario;
  bool estado;
  final String title;

  @override
  _PanelControlPage createState() => _PanelControlPage();
}

class _PanelControlPage extends State<PanelControlPage> {
  var estadoUsuario;
  //Timer timer;

  /* _read() async {
    final prefs = await SharedPreferences.getInstance();
    estadoUsuario = prefs.getBool('ingresos');
    print('read: $estadoUsuario');
  }*/

  @override
  void initState() {
    estadoUsuario = widget.estado;
    setState(() {
      //_read();
    });
    // timer = Timer.periodic(Duration(seconds: 3), (Timer t) => _read());
    super.initState();
    // _read();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(255, 255, 255, 1),
        child: ListView(
          children: <Widget>[
            wigdetPanelUsuario(),
            wigdetPanelTiendas(),
            wigdetPanelServicios(),
            widgetVolverseProveedor(),
          ],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }

  Widget wigdetPanelUsuario() {
    return Container(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
        child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  padding: const EdgeInsets.only(
                      left: 20.0, top: 20.0, bottom: 20.0),
                  width: MediaQuery.of(context).size.width,
                  color: Color.fromRGBO(220, 220, 220, 1),
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                      text: "Panel del Usuario",
                      style: TextStyle(
                          color: Color.fromRGBO(193, 29, 27, 1), fontSize: 25),
                    ),
                  )),
              Container(
                  width: MediaQuery.of(context).size.width,
                  padding:
                      const EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0),
                  child: RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FormularioPage(
                                username: widget.username,
                                password: widget.password,
                                idUsuario: widget.idUsuario,
                                token: widget.token,
                                status: "old")),
                      );
                    },
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Editar Perfil",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  )),
              Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  child: RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {},
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Cambiar Contraseña y/o Correo",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  )),
              Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  child: RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {},
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Ver mi Calendario",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  )),
              Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.only(
                      left: 20.0, bottom: 20.0, right: 20.0),
                  child: RaisedButton(
                    color: Color.fromRGBO(193, 29, 27, 1),
                    onPressed: () {
                      abrirPedidos();
                    },
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "Tus Pedidos",
                        style: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 18),
                      ),
                    ),
                  ))
            ],
          ),
        ));
  }

  Widget wigdetPanelTiendas() {
    if (estadoUsuario == true) {
      return Container(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
          child: Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                    padding: const EdgeInsets.only(
                        left: 20.0, top: 20.0, bottom: 20.0),
                    width: MediaQuery.of(context).size.width,
                    color: Color.fromRGBO(220, 220, 220, 1),
                    child: RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        text: "Panel de Tiendas",
                        style: TextStyle(
                            color: Color.fromRGBO(54, 185, 204, 1),
                            fontSize: 25),
                      ),
                    )),
                Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(
                        left: 20.0, top: 20.0, right: 20.0),
                    child: RaisedButton(
                      color: Color.fromRGBO(54, 185, 204, 1),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => NegociosPage(
                                idUsuario: widget.idUsuario,
                                token: widget.token
                              )),
                        );
                      },
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: "Tus Negocios Aquí!",
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 18),
                        ),
                      ),
                    )),
                Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: RaisedButton(
                      color: Color.fromRGBO(54, 185, 204, 1),
                      onPressed: () {                        
                        abrirOrdenes();
                      },
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: "Tus Ordenes",
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 18),
                        ),
                      ),
                    )),
                Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, bottom: 20.0),
                    child: RaisedButton(
                      color: Color.fromRGBO(54, 185, 204, 1),
                      onPressed: () {
                        abrirComentariosNegocio();
                      },
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: "Calificación y Comentarios",
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 18),
                        ),
                      ),
                    )),
              ],
            ),
          ));
    } else {
      return Container();
    }
  }

  Widget wigdetPanelServicios() {
    if (estadoUsuario == true) {
      return Container(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
          child: Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                    padding: const EdgeInsets.only(
                        left: 20.0, top: 20.0, bottom: 20.0),
                    width: MediaQuery.of(context).size.width,
                    color: Color.fromRGBO(220, 220, 220, 1),
                    child: RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        text: "Panel de Servicios",
                        style: TextStyle(
                            color: Color.fromRGBO(28, 200, 138, 1),
                            fontSize: 25),
                      ),
                    )),
                Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(
                        left: 20.0, top: 20.0, right: 20.0),
                    child: RaisedButton(
                      color: Color.fromRGBO(28, 200, 138, 1),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TiendaServiciosPage( idUsuario: widget.idUsuario,
                         token: widget.token)),
                        );
                      },
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: "Tus Servicios Aquí!",
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 18),
                        ),
                      ),
                    )),
                Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                    child: RaisedButton(
                      color: Color.fromRGBO(28, 200, 138, 1),
                      onPressed: () {

                      },
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: "Ver Calendario de Reservas",
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 18),
                        ),
                      ),
                    )),
                Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(
                        left: 20.0, right: 20.0, bottom: 20.0),
                    child: RaisedButton(
                      color: Color.fromRGBO(28, 200, 138, 1),
                      onPressed: () {
abrirComentariosTienda();
                      },
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: "Calificación y Comentarios",
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 18),
                        ),
                      ),
                    )),
              ],
            ),
          ));
    } else {
      return Container();
    }
  }

  widgetVolverseProveedor() {
    if (estadoUsuario != true) {
      return Container(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
          child: Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                    padding: const EdgeInsets.only(
                        left: 20.0, top: 20.0, bottom: 20.0),
                    width: MediaQuery.of(context).size.width,
                    color: Color.fromRGBO(220, 220, 220, 1),
                    child: RichText(
                      textAlign: TextAlign.left,
                      text: TextSpan(
                        text: "Tus negocios",
                        style: TextStyle(
                            color: Color.fromRGBO(54, 185, 204, 1),
                            fontSize: 25),
                      ),
                    )),
                Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(
                        left: 20.0, top: 20.0, right: 20.0),
                    child: RaisedButton(
                      color: Color.fromRGBO(54, 185, 204, 1),
                      onPressed: () {
                        setState(() {
                          _volverseProveedor();
                        });
                      },
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: "Oferta tus Productos y Servicios",
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1),
                              fontSize: 18),
                        ),
                      ),
                    )),
              ],
            ),
          ));
    } else {
      return Container();
    }
  }

  Future<void> _volverseProveedor() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('¿Quieres volverte proveedor?'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('¿Deseas ofertar tus productos y servicios?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Si'),
              onPressed: () {
                setState(() {
                  modificarPerfil();
                  Navigator.of(context).pop();
                  //Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePageLogin(username:widget.username, password: widget.password, token: widget.token, idUsuario: widget.idUsuario)), (Route<dynamic> route) => false);
                });
              },
            ),
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  modificarPerfil() async {
    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/me/perfil/' +
            widget.idUsuario.toString() +
            '/',
        headers: {
          'Authorization': "Token " + widget.token,
          'Content-type': 'application/json, charset: "utf-8'
        });
    var datas = utf8.decode(response.bodyBytes);
    var data = jsonDecode(datas);

    Map dataEdit = {
      'nombres': data["nombres"],
      'apellidos': data["apellidos"],
      'telefono': data["telefono"],
      'tipo_dni': data["tipo_dni"],
      'dni': data["dni"],
      'fecha_nacimiento': data["fecha_nacimiento"],
      'departamento': data["departamento"].toString(),
      'ciudad': data["ciudad"].toString(),
      'direccion': data["direccion"],
      'lon': data["lat"].toString(),
      'lat': data["lon"].toString(),
      "ingresos": "true",
    };

    var response1 = await http.put(
        'http://50.116.46.197:8000/api/1.0/me/perfil/update/' +
            widget.idUsuario.toString() +
            '/',
        body: dataEdit,
        headers: {'Authorization': "Token " + widget.token});
    var data1 = jsonDecode(response1.body);

    if (response.statusCode == 200) {
      setState(() {
        estadoUsuario = true;
      });

      final prefs = await SharedPreferences.getInstance();
      prefs.setBool('ingresos', true);
    }
  }

  abrirPedidos() async {
    List pedidos = [];
    final prefs = await SharedPreferences.getInstance();
    final myStringList = prefs.getStringList('datosInicio') ?? [];

    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/pedidos-cliente/',
        headers: {'Authorization': "Token " + myStringList[0]});

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      pedidos = data;
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PedidosClientePage(pedidos: pedidos)),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PedidosClientePage(pedidos: pedidos)),
      );
    }
  }

   abrirOrdenes() async {
    List pedidos = [];
    final prefs = await SharedPreferences.getInstance();
    final myStringList = prefs.getStringList('datosInicio') ?? [];

    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/listado-orden/',
        headers: {'Authorization': "Token " + myStringList[0]});

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      pedidos = data;
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OrdenesProveedorPage(pedidos: pedidos)),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => OrdenesProveedorPage(pedidos: pedidos)),
      );
    }
  }

  abrirComentariosNegocio() async {

    List idNegocios = [];
    List nombreNegocio =[];

    final prefs = await SharedPreferences.getInstance();
    final myStringList = prefs.getStringList('datosInicio') ?? [];

     final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/negocios/',
        headers: {'Authorization': "Token " + myStringList[0]});

        //print(response.body);
       var data = jsonDecode(response.body);

        for (var i = 0; i < data.length; i++){

          idNegocios.add(data[i]["id"]);
          nombreNegocio.add(data[i]["nombre"]);
          
        }

        
        
        print(idNegocios);
        print(nombreNegocio);

        Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ComentariosPage( nombreNegocio:nombreNegocio, idNegocio: idNegocios,  tipo: "negocio")));

      

  }

   abrirComentariosTienda() async {

    List idNegocios = [];
    List nombreNegocio =[];

    final prefs = await SharedPreferences.getInstance();
    final myStringList = prefs.getStringList('datosInicio') ?? [];

     final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/tienda-servicios/',
        headers: {'Authorization': "Token " + myStringList[0]});

        //print(response.body);
       var data = jsonDecode(response.body);

        for (var i = 0; i < data.length; i++){

          idNegocios.add(data[i]["id"]);
          nombreNegocio.add(data[i]["nombre"]);
          
        }

        
        
        print(idNegocios);
        print(nombreNegocio);

        Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ComentariosPage( nombreNegocio:nombreNegocio, idNegocio: idNegocios, tipo: "tienda")));

      

  }
}
