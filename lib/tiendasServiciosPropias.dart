import 'package:flutter/material.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:siguelo_app/crearServicios.dart';
import 'package:siguelo_app/crearTiendaServicio.dart';
import 'package:siguelo_app/productos.dart';
import 'package:siguelo_app/servicios.dart';
import 'crearNegocio.dart';
import 'crearProductos.dart';
import 'home.dart';

class TiendaServiciosPage extends StatefulWidget {
  TiendaServiciosPage({Key key, this.idUsuario, this.token}) : super(key: key);

  var idUsuario;
  String token;

  @override
  _TiendaServiciosPage createState() => _TiendaServiciosPage();
}

class _TiendaServiciosPage extends State<TiendaServiciosPage> {
  var info;

  Future placeInfo() async {
    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/tienda-servicios/',
        headers: {'Authorization': 'Token ' + widget.token});
    var data = jsonDecode(response.body);
    info = data;
    print(info.length);
  }

  @override
  void initState() {
    placeInfo().then((result) {
      setState(() {
        placeInfo();
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(255, 255, 255, 1),
        title: appBarMobile(),
        iconTheme: new IconThemeData(
          color: Color.fromRGBO(193, 29, 27, 1),
        ),
      ),
      body: SafeArea(
          child: Container(
        color: Color.fromRGBO(220, 220, 220, 1),
        child: Column(
          children: <Widget>[
            wigdetBtnCrear(),
            wigdetNegocios(),
          ],
        ),
      )),
    );
  }

  widgetLogo() {
    return Container(
        margin: const EdgeInsets.only(right: 30.0),
        width: 110.0,
        height: 30.0,
        decoration: new BoxDecoration(
            image: new DecorationImage(
                image: new AssetImage("assets/images/logo.png"),
                fit: BoxFit.fill)));
  }

  appBarMobile() {
    return Container(
        child: Row(children: <Widget>[
      widgetLogo(),
    ]));
  }

  Widget wigdetBtnCrear() {
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(left: 20.0, top: 20.0, right: 20.0),
        child: RaisedButton(
          color: Color.fromRGBO(193, 29, 27, 1),
          onPressed: () {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (BuildContext context) => CrearTiendaServiciosPage(
                        idUsuario: widget.idUsuario,
                        token: widget.token,
                        status: "crear",
                        infoNegocio: null)),
                (Route<dynamic> route) => false);
          },
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: "Registra tu tienda de servicios",
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1), fontSize: 18),
            ),
          ),
        ));
  }

  wigdetNegocios() {
    if (info.isNotEmpty) {
      return Expanded(
          child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: info.length,
              padding: const EdgeInsets.all(8),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                          color: Color.fromRGBO(54, 185, 204, 1),
                          width: MediaQuery.of(context).size.width,
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                              text: "Negocio: " + info[index]["nombre"],
                              style: TextStyle(
                                  color: Color.fromRGBO(255, 255, 255, 1),
                                  fontSize: 20),
                            ),
                          )),
                      Image.network(
                        info[index]["fachada"],
                      ),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: info[index]["direccion"],
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: "tel: " + info[index]["telefono"],
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                        ),
                      ),
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: info[index]["descripcion"],
                          style: TextStyle(
                              color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
                        ),
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: const EdgeInsets.only(
                              left: 20.0, top: 20.0, right: 20.0),
                          child: RaisedButton(
                            color: Color.fromRGBO(193, 29, 27, 1),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CrearServiciosPage(
                                        idUsuario: widget.idUsuario,
                                        token: widget.token,
                                        idNegocio: info[index]["id"],
                                        producto: null,
                                        status: "Crear")),
                              );
                            },
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text: "Crear servicio",
                                style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontSize: 18),
                              ),
                            ),
                          )),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding:
                              const EdgeInsets.only(left: 20.0, right: 20.0),
                          child: RaisedButton(
                            color: Color.fromRGBO(193, 29, 27, 1),
                            onPressed: () {
                              productos(info[index]["id"]);
                            },
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text: "Ver servicios",
                                style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontSize: 18),
                              ),
                            ),
                          )),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding:
                              const EdgeInsets.only(left: 20.0, right: 20.0),
                          child: RaisedButton(
                            color: Color.fromRGBO(193, 29, 27, 1),
                            onPressed: () {
                              Navigator.of(context).pushAndRemoveUntil(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          CrearTiendaServiciosPage(
                                              idUsuario: widget.idUsuario,
                                              token: widget.token,
                                              status: "editar",
                                              infoNegocio: info[index])),
                                  (Route<dynamic> route) => false);
                            },
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text: "Editar",
                                style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontSize: 18),
                              ),
                            ),
                          )),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding:
                              const EdgeInsets.only(left: 20.0, right: 20.0),
                          child: RaisedButton(
                            color: Color.fromRGBO(193, 29, 27, 1),
                            onPressed: () {
                                eliminarNegocio(info[index]["id"]);
                            },
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text: "Eliminar",
                                style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    fontSize: 18),
                              ),
                            ),
                          )),
                    ],
                  ),
                );
              }));
    } else {
      return Expanded(
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            text: "No tienes tiendas de servicio registradas!",
            style: TextStyle(color: Color.fromRGBO(0, 0, 0, 1), fontSize: 20),
          ),
        ),
      );
    }
  }

  productos(idNegocio) async {
    final response = await http.get(
        'http://50.116.46.197:8000/api/1.0/servicios/detail/' +
            idNegocio.toString() +
            '/',
        headers: {
          'Authorization': 'Token ' + widget.token,
          'Content-type': 'application/json'
        });
    var data = utf8.decode(response.bodyBytes);
    var productos = jsonDecode(data);
    print(productos);

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) =>
              ServiciosPage(productos: productos, token: widget.token, idUsuario: widget.idUsuario)),
    );
  }

  Future<void> eliminarNegocio(id) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Eliminar negocio'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('¿Desea eliminar el negocio?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Si'),
              onPressed: () {
                
                  
                  eliminar(id);
                
              
              },
            ),
            FlatButton(
              child: Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  eliminar(id) async {
      final response = await http.delete(
          'http://50.116.46.197:8000/api/1.0/tienda-servicios/' + id.toString() + '/',
          headers: {'Authorization': 'Token ' + widget.token});


          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => HomePageLogin( token: widget.token, idUsuario: widget.idUsuario)), (Route<dynamic> route) => false);

  }
}
